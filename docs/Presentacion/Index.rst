Presentación módulo
===================

El módulo profesional **Seguridad Informática** se imparte durante el segundo curso del Ciclo Formativo de Grado Medio de *Sistemas Microinformáticos y Redes* (Puedes encontrar más información en el `dossier del ciclo en la web de Conselleria <http://www.ceice.gva.es/es/web/formacion-profesional/publicador-ciclos/-/asset_publisher/FRACVC0hANWa/content/ciclo-formativo-sistemas-microinformatico-y-redes>`_.

.. list-table:: **CURSO 2024-2025**
   :widths: auto
   :header-rows: 0
   :align: center

   * - Tomás Giménez Albert
   * - `mt.gimenezalbert@edu.gva.es <mailto:mt.gimenezalbert@edu.gva.es/>`_
   * - *Martes 11:15 – 14:10 | Jueves 11:15 – 13:00*
   * - **5 H. Sem * 21 sem = 110 HORAS**

.. toctree::
   :maxdepth: 2

   Presentacion
