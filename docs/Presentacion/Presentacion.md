Calendario del curso
=====================
![](img/CalendariBotet.png "Calendario IES Botet Curso 2024-25")

Objetivos del módulo
====================
+ Aplicar medidas de seguridad pasiva en sistemas informáticos, describir características de entornos y relacionarlas con sus necesidades.
+ Gestionar dispositivos de almacenamiento, describir los procedimientos efectuados y aplicar técnicas para asegurar la integridad de la información.
+ Aplicar mecanismos de seguridad activa, describir sus características y relacionarlas con las necesidades de uso del sistema informático.
+ Asegurar la privacidad de la información transmitida en redes inalámbricas, describir las vulnerabilidades e instalar software específico.
+ Reconocer la legislación y normativa sobre seguridad y protección de datos, y analizar las repercusiones de su incumplimiento.

Contenidos
==========

<table style="margin-left: auto;margin-right: auto;padding:5px; border:1px solid;">
    <thead >
        <tr>
            <th>1ª Evaluación</th>
            <th>2ª Evaluación</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td >Introducción a la SI</td>
            <td >Seguridad en el SO. Seg. lógica.</td>
        </tr>
        <tr>
        <td >HW y Almacenamiento &nbsp;&nbsp;</td>
        <td>Seg en redes de ordenadores.</td>
        </tr>
        <tr>
            <td >Criptografía</td>
            <td></td<>
        </tr>
    </tbody>
</table>

Funcionamiento
===============

+ Asistencia **PRESENCIAL**
+ Responsabilidad y autonomía por vuestra parte.
+ Si algo no funciona. Se cambia. Se habla...
+ Ventaja: vamos a usar **herramientas que ya conocemos** y que no nos deben ser complicadas de usar. Además debemos ser capaces de usar cada una correctamente.
  + **Mestre a Casa** → Recordad acceso. ¿Mayores de edad?
  + Aules. → Materiales, tareas, cuestionarios, **foros**
  + Correo **@edu.gva.es** que incluye herramientas como:
    + Onedrive → Almacenamiento en la nube.
    + Teams → Videoconferencia.
    + Calendario (Personal/Del grupo de la asignatura para exámenes...)
+ Telegram (grupo para noticias/anuncios) → [Enlace al canal](https://t.me/+JNyTUY8d7wJlOWY0)

  ![QRGrupoTelegram](img/QRGrupoTelegram.jpg "Enlace al canal de Telegram")

+ Las explicaciones teóricas se harán en clase con lo más reducidas posible y a trabajar (**en clase y en casa**).
+ A través de los medios anteriores o en clase se podrán resolver las dudas que vayan apareciendo.
+ Los puestos están señalados. NO desplazar equipo o mobiliario.
+ Siempre nos sentaremos en el mismo sitio
+ Para evitar las pérdidas de información y facilitar tu trabajo tienes dos opciones (**OBLIGATORIO**):
  + Usa tu propio disco duro externo/portátil SSD (seguro que será más rápido que el DD del PC del aula).
  + Traer tu propio portátil (En este caso habría que ubicarte en algún puesto concreto. **ATENCIÓN AL USO DE LOS CABLES DE RED**).
+ **Atención con la UBICACIÓN de tus MV si están en el DD del PC.**
+ **Atención a las entradas/salidas de clase/patios**
+ **Este curso estrenamos PDIs**. No acercarse a ellas bajo ningún concepto (sanción disciplinaria).

Evaluación
===============

+ Prácticas / Ejercicios=40% → **La parte más sencilla para obtener nota**
+ Exámenes por tema/s=30%
+ Examen Final Evaluación=30%
+ Aprobar una evaluación significa superar esa parte del módulo. En caso contrario se debe recuperar en la siguiente evaluación.
