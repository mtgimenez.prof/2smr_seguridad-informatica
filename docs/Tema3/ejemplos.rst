Ejemplos de uso
=================
Ejemplos de herramientas criptográficas para cifrar, firmar, etc...encontramos en nuestro día a día. A continuación tienes dos ejemplos.

Certificados digitales
-----------------------
El certificado digital es un documento que contiene fundamentalmente información sobre una persona o entidad y una clave pública y una firma digital de un organismo de confianza (autoridad certificadora) que rubrica que la clave pública que contiene el certificado pertenece al propietario del mismo. Para cumplir la función de identificación y autenticación necesita del uso de la clave privada. El certificado y la clave pública pueden distribuirse a terceros.(X.509)

**COMPONENTES**

* Versión,número de serie.
* Algoritmo de firma (X.509) .
* La autoridad certificadora (emisor).
* El periodo de validez.
* Propietario de la clave (asunto).
* La clave pública.
* La firma digital de la autoridad certificadora.

.. image:: img/certificados.png
      :alt: Certificados digitales
      :width: 400 px
      :align: center

El contenido fundamental del certificado es el ASUNTO y la clave pública (identidad y clave pública asociada), la firma de la autoridad y el algoritmo son datos imprescindibles para poder validar el certificado.

DNIe
-----

El DNI Electrónico (DNIe) es un mecanismo para identificarnos digitalmente en multitud de herramientas, mediante técnicas de criptografía. Es un ejemplo claro de **PKI (Public Key Infraestructure) o infraestructura de clave pública**.

Una PKI incluye a todo el hardware y software necesario para las comunicaciones seguras mediante el uso de certificados digitales y firmas digitales.

**COMPONENTES**

1. La autoridad de certificación, CA (Certifica te Authority)  emitir y revocar los certificados.
2. La autoridad de registro, RA (Registration Authority) controlar la generación de certificados.Verifica el enlace entre los certificados (concretamente, entre la clave pública del certificado) y la identidad de sus titulares.
3. La autoridad de validación, VA (Validation Authority): comprobar la validez de los certificados digitales.
4. Software necesario para poder utilizar los certificados digitales.
5. Política de seguridad definida para las comunicaciones (algoritmos...).

.. image:: img/dnie.png
      :alt: Dni Electrónico
      :width: 500 px
      :align: center


.. note::

    ¿Sabrías encontrar un listado de autoridades de certificación existentes en España?
