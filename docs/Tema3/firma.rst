Firma digital
=============

Debemos distinguir entre:

      * **Cifrar**: encriptamos un mensaje para asegurar su confidencialidad (sólo lo ve quién debe verlo).
      * **Firmar**: demuestra la autenticidad del emisor (soy quién digo ser).

La primera utilidad de la criptografía es garantizar la **confidencialidad** de la comunicación cifrando el documento o mensaje original.

La segunda utilidad es la de **autenticar** al emisor.

Muchos de los programas que utilizamos habitualmente permiten firmar digitalmente los documentos, por ejemplo:

      * LibreOffice
      * Acrobat Reader


En la criptografía asimétrica el mecanismo de firma es el encargado de garantizar que el emisor del documento o mensaje es quien dice ser, para ello el emisor aplica al documento un resumen (hash) que genera una serie de caracteres resumen que sólo se pueden haber obtenido con el documento original.

.. warning::

   Aquí utilizamos la clave privada para cifrar y no la pública, así demostramos quién es el emisor.


.. image:: img/firmadigital.png
    :width: 600 px
    :alt: Proceso de firma digital y verificación.
    :align: center


1. **El emisor**

    * Aplica la función hash al documento original (resumen).
    * Encripta el resumen con su clave privada, que sólo tiene él (firma).
    * Envía el documento original y la firma al receptor.

2. **El receptor**

    * Aplica la función hash al documento original para obtener el resumen (A).
    * Desencripta la firma con la clave pública del emisor para obtener el resumen (B).
    * Si el documento A es igual a B entonces el emisor es quién dice ser.

.. raw:: html

        </br>
        <div style="text-align: justify; color: orange; background-color: #e0e0e0; border-radius: 25px; padding-top: 20px;padding-right: 30px;padding-bottom: 20px; padding-left: 30px;">
        <u><b>PRÁCTICA 2</b></u></br>
        Vas a comprobar el origen de un documento a través de la firma digital.
        </div>
