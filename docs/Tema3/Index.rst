Criptografía
=============
El perfeccionamiento de las REDES DE ORDENADORES y la multitud de aplicaciones y servicios que ofrecen actualmente han hecho obligatorio el uso de los sistemas de cifrado, de forma que un mensaje después de un proceso de transformación, lo que llamamos cifrado, solo pudiera ser leído siguiendo un proceso de descifrado.
Pensando en los objetivos de la Seguridad Informática, podríamos asociar estas técnicas dentro de:

    * Confidencialidad.
    * Integridad.
    * Disponibilidad

Desde que el hombre es capaz de comunicarse por escrito, ha tenido la necesidad de preservar la privacidad de la información en la transmisión de mensajes confidenciales entre el emisor y el receptor.

.. image:: img/escitala.png
      :alt: Ejemplo escitala griega
      :width: 400 px
      :align: center

A lo largo de la historia, las distintas civilizaciones y culturas han ido creando nuevas formas de criptografía:

    * ESCÍTALA
    * POLYBIOS
    * CIFRADO DEL CESAR
    * CIFRADOR DE VIGENÈRE
    * `ENIGMA <https://es.wikipedia.org/wiki/Enigma_(m%C3%A1quina)>`_. Puedes encontrar incluso `simuladores de máquinas Enigma <https://www.101computing.net/enigma-machine-emulator/>`_ en la web.


La criptografía es un proceso que nos rodea en la mayoría de actividades que realizamos diariamente:

    * **Navegar por la web (HTTPS)**
    * **Conectarnos a una red wifi (wpa/wpa2...)**

En la mayoría de ocasiones, los sistemas criptográficos, se apoyan en dos elementos:

    1. Un **algoritmo**: conocido, dada una entrada solo puede devolver una salida y dada una salida no es posible resolver la entrada.
    2. Una **clave**: combinaciones de símbolos. Son el objetivo de los ataques por fuerza bruta. Algunas recomendaciones (política de seguridad):
          * Longitud
          * Cambio regular
          * Complejidad

.. raw:: html

   <div style="text-align:center;">
   <a title="MesserWoland, CC BY-SA 3.0 &lt;http://creativecommons.org/licenses/by-sa/3.0/&gt;, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Crypto_key.svg"><img width="100" alt="Crypto key" src="https://upload.wikimedia.org/wikipedia/commons/thumb/6/65/Crypto_key.svg/512px-Crypto_key.svg.png"></a>
   </div>

Para estudiar esta herramienta fundamental de la seguridad Informática, vamos  a revisar los siguientes apartados:

.. toctree::
   :maxdepth: 2

   tecnicas
   algoritmos
   herramientas
   resumen
   firma
   ejemplos
