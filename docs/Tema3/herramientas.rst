Herramientas
=============

Podemos encontrar muchísimas utilidades para poder encriptar/desencriptar información en cualquiera de los sistemas operativos existentes.

**WINDOWS**

Se puede aplicar **criptografía simétrica** a ficheros a través de multitud de programas:
      * `VeraCrypt <https://www.veracrypt.fr/en/Home.html>`_
      * TrueCrypt

o incluso aprovechando las opciones de algún compresor tipo `Winrar <https://www.winrar.es/soporte/compresion/49/como-proteger-un-archivo-con-contrasena>`_.

.. raw:: html

    <p>
      También existen programas que ofrecen una funcionalidad mucho más completa, como <a href="https://www.gpg4win.org/" target="_blank">GpG4Win</a>, el cual hace uso del protocolo OpenPGP<sup id="fnref:note1"><a class="footnote-ref" href="#fn:note1" role="doc-noteref">1</a></sup>.
    </p>



.. raw:: html

    <p style="text-align: justify;"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/4/42/Pdf-2127829.png/480px-Pdf-2127829.png" alt="Perfil" width="50" style="vertical-align: middle; float:left;"/>  Puedes acceder al siguiente documento para conocer un poco mejor como funciona el programa GpG4Win. También encontrarás en Internet muy buena documentación. </br> </br>

.. image:: img/tutorial-gpg4win.pdf
    :width: 400 px
    :alt: Manual para crear Llaves Privadas y Públicas en Windows.
    :align: center


**LINUX**

.. raw:: html

    <p>
      En los sistemas operativos Unix/Linux se suele utilizar la herramienta <a href="https://gnupg.org/" target="_blank">GnuPG(GNU Privacy Guard)</a>, también una implementación LIBRE del protocolo OpenPGP<sup id="fnref:note1"><a class="footnote-ref" href="#fn:note1" role="doc-noteref">1</a></sup>, y que viene instalada en la mayoría de distribuciones Linux.
    </p>

.. raw:: html

    <p style="text-align: justify;"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/4/42/Pdf-2127829.png/480px-Pdf-2127829.png" alt="Perfil" width="50" style="vertical-align: middle; float:left;"/>  El uso de GnuPG es muy sencillo, accede al manual elaborado para conocer como realizar encriptación con este programa en las dos técnicas existentes. </br> </br>

.. image:: img/manualGPG.pdf
    :width: 400 px
    :alt: Cifrado simétrico y asimétrico con GPG.
    :align: center

Entre las muchas opciones de GPG destacan las siguientes:

+-----------------+-----------------------------------------------+
| Opción          | Significado                                   |
+=================+===============================================+
| **-c**          | Cifrar con un algorítmo simétrico             |
+-----------------+-----------------------------------------------+
|  **-a**         | Produce una salida ASCII codificada en BASE64 |
+-----------------+-----------------------------------------------+
| **--gen-key**   | Genera un par de claves                       |
+-----------------+-----------------------------------------------+
| **--export**    | Exporta una o más claves públicas             |
+-----------------+-----------------------------------------------+
| **--import**    | Importa las claves públicas a nuestro keyring |
+-----------------+-----------------------------------------------+
| **--encrypt**   | Cifrar con criptografía asimétrica            |
+-----------------+-----------------------------------------------+
| **-s**          | Firma digitalmente un documento               |
+-----------------+-----------------------------------------------+
| **-clearsign**  | Firma sin cifrar                              |
+-----------------+-----------------------------------------------+


.. note::

   ¿Sabrías realizar la encriptación de manera simétrica tanto en Windows(por ejemplo con WinRar), como en Linux con GPG?




Si te das cuenta, en ambos sistemas operativos se ha hablado del mismo estándar de encriptación **(OpenPGP)**, por lo tanto puedes intercambiar información encriptada independientemente del SO en el que estés trabajando.


.. raw:: html

        </br>
        <div style="text-align: justify; color: orange; background-color: #e0e0e0; border-radius: 25px; padding-top: 20px;padding-right: 30px;padding-bottom: 20px; padding-left: 30px;">
        <u><b>PRÁCTICA 1</b></u></br>
        Realiza la práctica 1 del Tema 3 del aula virtual, vas a realizar un intercambio seguro de información <u>entre los dos sistemas operativo</u> que usamos en el curso.
        </div>

.. raw:: html

   </br>
   <div class="footnotes">
       <hr />
       <ol>
           <li class="footnote" id="fn:note1">
               <p>
                   <b>OpenPGP:</b> <a href="https://www.openpgp.org/" target="_blank">Estándar NO PROPIETARIO para encriptación</a> <a class="footnote-backref" rev="footnote" href="#fnref:note1">&#8617;</a>
               </p>
           </li>
       </ol>
   </div>
