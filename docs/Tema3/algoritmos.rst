Algoritmos de cifrado
======================

Tenemos multitud de ejemplos de todos los tipos de criptografía que hemos visto. No entraremos al detalle de funcionamiento de cada uno, pero si es interesante conocer algunos de ellos, ya que presentan características distintas.


**SIMÉTRICOS**

* DES
* RC5
* AES
* Blowfish
* IDEA

**ASIMÉTRICOS**

* DSA
* RSA
* ELGAMAL
