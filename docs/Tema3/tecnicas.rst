Técnicas criptográficas
========================

.. raw:: html

    <p>
      Dependiendo del algoritmo y del tipo de claves que se utilice para realizar el cifrado de nuestra información podemos agrupar las técnicas criptográficas en 3 grandes grupos <sup id="fnref:note1"><a class="footnote-ref" href="#fn:note1" role="doc-noteref">1</a></sup>:
    </p>


Criptografía simétrica
-----------------------
Este método se basa en un secreto compartido entre la entidad que cifra el mensaje y la que lo quiere descifrar, usando la misma clave en TODO EL PROCESO

      .. image:: img/CriptografiaSimetrica.png
            :alt: Criptografía simétrica
            :width: 400 px
            :align: center

      **VENTAJAS**

      * Emplean algoritmos muy eficientes, pues sólo emplean una clave cuyos resultados son reversibles.
      * Son muy sencillos de emplear por los usuarios, pues las clave secreta pueden ser palabras de paso recordables.

      **INCONVENIENTES**

      * Cómo ponemos de acuerdo al emisor y al receptor para que empleen la misma clave?
      * Cada par de usuarios que se comunican requieren de una clave ⇒ en un sistema con muchos usuarios se generan muchas claves ⇒ ¿Cómo y dónde almacenamos las claves?

Puedes ver con más detalle esto en el siguiente video:

.. raw:: html

              <iframe width="250" style="display:block; margin-left:auto; margin-right:auto;" src="https://www.youtube.com/embed/46Pwz2V-t8Q" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


Criptografía asimétrica
-----------------------

También denominada CRIPTOGRAFÍA DE CLAVE PÚBLICA. Cada una de las partes tiene una pareja de claves. Una pública, que deberá intercambiar las entidades con las que quiera comunicarse, y otra privada, y que jamás debe comunicar a nadie. Para cifrar un mensaje, el emisor utilizará la clave pública del receptor, y a su vez, el receptor descifrará este mensaje haciendo uso de su clave privada. Las dos claves se generan a la vez y se encuentran relacionadas matemáticamente entre sí mediante funciones de un solo sentido; es imposible descubrir la clave privada a partir de la pública.

      .. image:: img/CriptografiaAsimetrica.png
            :alt: criptografía asimétrica
            :width: 500 px
            :align: center


      **VENTAJAS**

      * Soluciona el problema del número de claves: pasamos de :math:`n*(n-1)` claves (siendo **n** el número de usuarios del criptosistema) a :math:`2n`.
      * Soluciona el problema de la difusión de claves que había en los criptosistemas simétricos.


      **INCONVENIENTES**

      .. raw:: html

            <ul>
            <li>Las claves deben ser suficientemente largas para que sean matemáticamente independientes (de hecho, no son claves que puedan recordarse).</li>
            <li>Los algoritmos son muy poco eficientes.</li>
            <li>Como la clave privada es irrecordable, debe almacenarse en algún gestor de contraseñas, y éste deberá estar bien protegido de alguna manera ⇒ keyring<sup id="fnref:note2"><a class="footnote-ref" href="#fn:note2" role="doc-noteref">2</a></sup></li>
            </ul>

Criptografía híbrida
-----------------------

 Para poder aprovechar las ventajas de ambos tipos de criptografía se adopta un solución **híbrida**. Consistiría en utilizar **criptografía de clave privada para intercambiar mensajes**, pues éstos son más pequeños y además el proceso es rápido, y utilizar **criptografía de clave pública para el intercambio de las claves privadas**.

 Uno de los ejemplos más habituales lo tenemos en el uso del protocolo SSH para realizar conexiones a equipos remotos. Usando la siguiente estrategia:

    1. Se usa criptografía asimétrica solo para el inicio de la sesión, cuando hay que generar un canal seguro donde acordar la clave simétrica aleatoria que se utilizará en esa conversación.
    2. La criptografía simétrica se usa durante la transmisión utilizando la clave simétrica acordada durante el inicio de sesión, la cual se puede variar cada cierto tiempo para dificultar el espionaje de la conversación.

.. image:: img/criptohibrida.png
            :alt: Criptografía simétrica
            :width: 400 px
            :align: center

.. raw:: html

   </br>
   <div class="footnotes">
       <hr />
       <ol>
           <li class="footnote" id="fn:note1">
               <p>
                   <b>Fuente:</b> <a href="https://www.genbeta.com/desarrollo/tipos-de-criptografia-simetrica-asimetrica-e-hibrida" target="_blank">Web con una explicación bastante completa</a> <a class="footnote-backref" rev="footnote" href="#fnref:note1">&#8617;</a>
               </p>
           </li>
           <li class="footnote" id="fn:note2">
               <p>
                   <b>Más información:</b> <a href="https://en.wikipedia.org/wiki/List_of_password_managers" target="_blank">Listado de gestores de contraseñas y keyring</a> <a class="footnote-backref" rev="footnote" href="#fnref:note2">&#8617;</a>
               </p>
           </li>
       </ol>
   </div>
