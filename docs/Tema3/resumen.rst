Función resumen
================

También denominadas funciones **hash o digest**, obtienen de un conjunto de datos de entrada, una salida uníca. Esta salida se denomina hash, resumen o checksum, y se suele utilizar para asegurar que algo no ha sufrido ninguna variación.

.. warning::

    1. El programa GPG4Win incluye funcionalidades de resumen.
    2. En Linux tenemos, por ejemplo, el comando **MD5SUM**

**EJEMPLOS DE USO**

    * Verificar descargas
    * Verificar contenidos de ficheros
    * Verificar firmas digitales
    * Verificar transacciones en criptomoneda

**ALGORITMOS**

    * MD5
    * SHA
    * Tiger

.. image:: img/funcionresumen.png
    :width: 400 px
    :alt: Funcion resumen.
    :align: center

.. note::

    ¿Sabrías asegurar si un fichero original y uno de copia son iguales, tanto en Windows como en Linux?
