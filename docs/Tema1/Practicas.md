Buenas Prácticas
================
Las buenas prácticas, sea cual sea el ámbito del que hablemos, se refieren a todos los elementos que nos van a permitir mejorar en nuestro empeño en dicho ámbito, como pueden ser:
  * Herramientas
  * Estrategias
  * Metodologías

Si anteriormente hemos hablado de estándares ([ISO 27001](https://www.aenor.com/certificacion/tecnologias-de-la-informacion/seguridad-informacion)) como algo útil para la mejora, parte de esos estándares incluyen, de una manera u otra, la aplicación de buenas prácticas para cumplir con lo especificado en el estándar en cuestión. El trabajo sobre este concepto nos puede ayudar a crear materiales y recursos para realizar una correcta fase de **formación y motivación** en nuestro entorno.

<div style="text-align:center;">
<a title="Best practice password policy" href="https://www.comtact.co.uk/wp-content/uploads/2020/06/Best-Practice-Password-Policy-FINAL-070519-scaled.jpg" target="_blank"><img width="30%" alt="Best practice password policy" src="https://www.comtact.co.uk/wp-content/uploads/2020/06/Best-Practice-Password-Policy-FINAL-070519-scaled.jpg"></a>
</div>

Existen **multitud de recursos en la web** donde puedes encontrar buenas prácticas en el campo de la seguridad informática (o **ciberseguridad**), destacando entre ellas la [página del INCIBE(Instituto Nacional de Ciberseguridad)](https://www.incibe.es/protege-tu-empresa/que-te-interesa/buenas-practicas-area-informatica) dedicada a las buenas prácticas. Allí podrás encontrar:

* Infografías (**como la imagen anterior**)
* Guías
* Listado de empresas dedicadas a este campo
* .....

<div style="text-align: justify; color: orange; background-color: #e0e0e0; border-radius: 25px; padding-top: 20px;padding-right: 30px;padding-bottom: 20px; padding-left: 30px;">
<u><b>Ejercicio 3</b></u></br>
Realiza el tercer ejercicio de este tema, ubicado en el aula virtual. Vas a diseñar una <b>INFOGRAFÍA</b> para ayudar a l@s emplead@s de tu empresa a cumplir con la normativa de seguridad informática. Envía un <b>documento pdf</b> con tu solución.
</div>
