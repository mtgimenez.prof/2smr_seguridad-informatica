Objetivos De La Seguridad Informática
======================================

Proteger los **4 elementos** más importantes de los sistemas de información.
<ol type="A">
  <li>INFORMACIÓN</li>
  <li>MATERIAL</li>
  <li>REDES Y COMUNICACIONES</li>
  <li>USUARI@S</li>
</ol>

___
> **¿SABRÍAS?...**
Imaginando un entorno o situación que incluya un sistema informático. ¿Serías capaz de realizar un infograma con una herramienta tipo Canva en el que muestres cada uno de los elementos anteriores en el ejemplo que has pensado?
___

<p>
    Según los estándares actuales, la seguridad en un sistema informático se puede caracterizar por la preservación de <b><sup id="fnref:note1"><a class="footnote-ref" href="#fn:note1" role="doc-noteref">1</a></sup></b> las siguientes características:
</p>

1. CONFIDENCIALIDAD.
2. INTEGRIDAD
3. DISPONIBILIDAD.
4. AUTENTICACIÓN
5. IRREFUTABILIDAD (No-Rechazo o No-Repudio)

___
> **¿SABRÍAS?...**
¿Definir con tus propias palabras cada una de las características de un sistema informático seguro?
___

<div style="text-align: justify; color: BLUE; background-color: #e0e0e0; border-radius: 25px; padding-top: 20px;padding-right: 30px;padding-bottom: 20px; padding-left: 30px;">
La filosofía que debe seguir un SI seguro es que TODO LO QUE NO ESTÁ PERMITIDO DEBE ESTAR PROHIBIDO
</div></br>


Para conseguir los objetivos anteriores, **marcados por los estándares**, se utilizan mecanismos como:

+ **Autenticación**, que permite identificar al emisor de un mensaje, al creador de un documento o al equipo que se conecta a una red o a un servicio.
+ **Autorización**, que controla el acceso de los usuarios a zonas restringidas, a distintos equipos y servicios después de haber superado el proceso de autenticación.
+ **Sistemas de alimentación ininterrumpida (SAI)**, que garantizan la continuidad en el suministro eléctrico en caso de caida de la red.
+ **Encriptación**, que ayuda a ocultar la información transmitida por la red o almacenada en los equipos
+ Sistemas de copias de seguridad(**backup**)
+ Mecanismos de **redundancia**(RAID, Clusters)
+ **Antivirus**
+ **Cortafuegos o firewall**, programa que audita y evita los intentos de conexión no deseados en ambos sentidos, desde los equipos hacia la red y viceversa.
+ **Servidores proxys**, consiste en ordenadores con software especial, que hacen de intermediario entre la red interna de una empresa y una red externa, como pueda ser Internet

___
> **¿SABRÍAS?...**
1. ¿Qué es un estándar?
2. ¿Que tipos hay?
3. ¿Cual es el conjunto que regula la seg. de los sistemas de información?
4. ¿Crees que los estándares son algo útil?
___

</br>
<div class="footnotes">
    <hr />
    <ol>
        <li class="footnote" id="fn:note1">
            <p>
                <b>Fuente:</b> <a href="https://udit.es/actualidad/seguridad-informatica-que-es-y-tipos/" target="_blank">UDIT(Universidad de Diseño, innovación y tecnología)</a> <a class="footnote-backref" rev="footnote" href="#fnref:note1">&#8617;</a>
            </p>
        </li>
    </ol>
</div>
