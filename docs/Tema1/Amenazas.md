Amenazas En Los Sistemas De Información
=======================================
El objetivo final de la seguridad es proteger lo que la empresa posee. (Recordar los [4 ELEMENTOS vistos al principio del tema](Objetivos)).Cualquier daño que se produzca sobre estos activos tendrá un impacto en la empresa.
<div style="text-align: justify; color: BLUE; background-color: #e0e0e0; border-radius: 25px; padding-top: 20px;padding-right: 30px;padding-bottom: 20px; padding-left: 30px;">
LA SEGURIDAD TOTAL NO EXISTE, pero un buen plan de seguridad minimiza los riesgos y los posibles daños.
</div></br>

**Pasos a seguir para mejorar la seguridad**

1. Identificar los activos.
2. Evaluar los riesgos, considerando el impacto que pueden tener
3. Diseñar el plan de actuación
4. Formación/ Concienciación

![Pasos a seguir](img/pasos.png "Pasos a seguir")

___
> **¿SABRÍAS?...**
¿Definir con tus propias palabras cada uno de estos términos?
* AUDITORIA
* POLÍTICA DE SEGURIDAD
* PLAN DE CONTINGENCIAS
___

## Vulnerabilidades

Las vulnerabilidades son el resultado de un fallo o deficiencia durante el proceso de creación de programas de ordenador o computadora (software). Pueden ser más o menos **actuales**, **conocidas** y con distintos **niveles de gravedad**

<div style="text-align: justify; color: orange; background-color: #e0e0e0; border-radius: 25px; padding-top: 20px;padding-right: 30px;padding-bottom: 20px; padding-left: 30px;">
<u>Ejercicio 1</u></br>
Busca un listado de vulnerabilidades en la web. Explica qué encuentras sobre ellas y donde lo encuentras. Incluye imágenes si lo deseas.
Debes <b>entregar el documento en la tarea Vulnerabilidades Conocidas</b>, del aula virtual.
</div>

## Tipos de amenazas

Las vulnerabilidades son el resultado de un fallo o deficiencia durante el proceso de creación de programas de ordenador o páginas web o cualquier otro software. También puede estar relacionado con una mala elección o configuración del hardware.

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;margin:0px auto;}
.tg td{border-color:grey;border-style:dotted;border-width:1px;
  overflow:hidden; word-break:normal;}
.tg th{border-color:grey;border-style:dotted;border-width:1px;font-weight: bold;overflow:hidden;padding:1px 5px;word-break:normal; }
.tg .tg-0pky{border-color:inherit;text-align:left;vertical-align:top}
</style>
<table class="tg">
<thead>
  <tr>
    <th class="tg-0pky">ATACANTES</th>
    <th class="tg-0pky">ATAQUES</th>
    <th class="tg-0pky">MÉTODOS</th>
</thead>

  </tr>
</thead>
<tbody>
  <tr>
    <td class="tg-0pky">HACKERS</td>
    <td class="tg-0pky">INTERRUPCION</td>
    <td class="tg-0pky">SPOOFING</td>
  </tr>
  <tr>
  <td class="tg-0pky">CRACKES</td>
  <td class="tg-0pky">INTERCEPTACION</td>
  <td class="tg-0pky">SNIFFING</td>
  </tr>
  <tr>
  <td class="tg-0pky">SNIFFERS</td>
  <td class="tg-0pky">MODIFICACION</td>
  <td class="tg-0pky">KEYLOGGERS</td>
  </tr>
  <tr>
  <td class="tg-0pky">LAMMERS</td>
  <td class="tg-0pky">INTERRUPCION</td>
  <td class="tg-0pky">DENEGACION DE SERVICIO</td>
  </tr>
  <tr>
  <td class="tg-0pky">PROG. VIRUS</td>
  <td class="tg-0pky">DESTRUCCIÓN</td>
  <td class="tg-0pky">PHISING</td>
  </tr>
</tbody>
</table>
</br>
<div style="text-align: justify; color: orange; background-color: #e0e0e0; border-radius: 25px; padding-top: 20px;padding-right: 30px;padding-bottom: 20px; padding-left: 30px;">
<u>Ejercicio 2</u></br>
Completa el <b>ejercicio Tipos de atacantes y métodos utilizados</b>, que se encuentra en el aula virtual. Debes buscar las definiciones de 3 elementos de ambas columnas.
</div>
</br>

## Ejemplo

La mayoría de ataques a los Sistemas Informáticos actuales están relacionados con la conectividad a la red (Internet), ya que en este caso, nuestra LAN(Red de Area Local), puede estar expuesta a inspecciones no deseadas, provenientes tanto de nuestra propia red, como desde el exterior, utilizando herramientas como:
* Sniffers/analizadores de red → Wireshark ([Enlace a manual](https://profesorcyber.blogspot.com/2018/04/uso-basico-de-wireshark-parte-1.html)). Puedes encontrar muchos videotutoriales, como por ejemplo:
    + [Fitrado de paquetes <b>ping</b>](https://youtu.be/BJ1ShZpzy9k)
    + [Fitrado de paquetes <b>http</b>](https://youtu.be/8bqmkA1_w5U)
* Escaneadores de puertos → Nmap/Zenmap

<div style="text-align: justify; color: orange; background-color: #e0e0e0; border-radius: 25px; padding-top: 20px;padding-right: 30px;padding-bottom: 20px; padding-left: 30px;">
<u><b>PRÁCTICA 1</b></u></br>
Realiza la práctica 1 de este tema, ubicada en el aula virtual. Sigue el enunciado y envía un <b>documento pdf</b> con tu solución.
</div>
