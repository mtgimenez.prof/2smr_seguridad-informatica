El ciclo de vida de la seguridad informática
============================================
La ISO/IEC 27001 define la seguridad como un proceso de mejora continua. La seguridad informática es un proceso que comienza en el momento en que se implanta en la organización, pero que nunca finaliza. Las actuaciones que hagamos hoy, puede que no sirvan para un futuro cercano. La tecnología avanza muy rápidamente y de la misma forma que aparecen nuevas funcionalidades y maneras de hacer las cosas, surgen como consecuencia nuevas amenazas y vectores de ataque. Por tanto, si somos los responsables de seguridad de una organización, debemos estar continuamente aplicando las fases del ciclo de vida de la seguridad:

![Fases del ciclo de vida de la seguridad informática PDCA!](./img/PDCA.jpeg "Fases del ciclo de vida de la seguridad informática PDCA")

También se le conoce como el modelo Plan-Do-Check-Act (PDCA o ciclo Demming) y que consiste en las siguientes fases:

**Planificación (Plan)**

Establecer la política, objetivos,  procesos y procedimientos relativos a la gestión del riesgo y mejorar la seguridad de la información de la organización para ofrecer resultados de acuerdo con las políticas y objetivos generales de la organización.

Identificar lo que se quiere mejorar.
Recopilar datos del proceso que se quiere mejorar.
Analizar los datos recogidos.
Establecer los objetivos de mejora.
Detallar los resultados esperados.
Definir los procesos necesarios conseguir los objetivos.

**Ejecución (Do)**

Implementar y gestionar el Sistema de Gestión de la Seguridad de la Información (SGSI) de acuerdo a su política, controles, procesos y procedimientos. En la medida de lo posible debería hacerse en un entorno de prueba para poder verificar sus resultados antes de implantarlo en el sistema real.

**Seguimiento (Check)**

Verificar, medir y revisar las prestaciones de los procesos del SGSI. Comprobar que las medidas adoptadas han surtido efecto, para ello se debe volver a recopilar datos y monitorizar el comportamiento del sistema.

**Mejora (Act)**

Adoptar acciones correctivas y preventivas basadas en auditorías y revisiones internas con el objetivo de mejorar el SGSI. Hace referencia a la actitud que se debe tomar después de los tres primeros pasos y dependerá de lo que haya ocurrido. En caso de haber ocurrido algún mal funcionamiento, se deberá repetir el ciclo de nuevo. Si el funcionamiento ha sido correcto, se instalarán las modificaciones en el sistema de manera definitiva.


La seguridad debe considerarse en toda organización como una inversión y no como un gasto. Desgraciadamente no todas las organizaciones se han dado cuenta de eso y es fácil ver en la actualidad muchas empresas sin políticas de seguridad, sin planes de contingencia o continuidad de negocio ante desastres o incumpliendo la Ley Orgánica de Protección de Datos. Sobretodo esto es más común en las pequeñas y medianas empresas.

Muchas orgaizaciones reaccionan cuando han sufrido un incidente de seguridad y no tienen planes de acción de forma que lo dejan todo a la improvisación. No tener un SGSI implantado puede llevar a ocasionar muchas pérdidas económicas e incluso la reputación y la imagen de la empresa ante un incidente de seguridad con gran impacto y que pueda ser anunciado en los medios de comunicación, dando una mala publicidad a la empresa.

También es verdad es que empieza a haber un cambio de tendencia en el sector y las organizaciones empiezan a demandar cada vez más profesionales en el campo de la seguridad informática para ayudarles a implantar un SGSI o auditar sus sistemas.

Como ejemplo, léanse las siguientes noticias:

<ul>
 <li><a href="https://www.ciospain.es/seguridad/cisco-predice-una-escasez-global-de-profesionales-de-seguridad" target="_blank">Cisco predice una escasez de profesionales de seguridad</a></li>
  <li><a href="https://www.pandasecurity.com/es/mediacenter/seguridad/especialistas-ciberseguridad/" target="_blank">Hacen falta un millón de especialistas en seguridad en todo el mundo</a></li>
  <li><a href="https://www.expansion.com/economia-digital/companias/2016/01/21/56a10e3f22601d77548b4628.html" target="_blank">Las empresas suspenden en ciberseguridad</a></li>
</ul>

Para finalizar, las organizaciones deben plantearse muy seriamente la necesidad de formación y concienciación de sus empleados en materia de seguridad informática, ya que el factor humano es muy importante a la hora de evitar incidentes. Muchos de los problemas se derivan por malas prácticas en los empleados y por desconocimiento de los peligros que acechan en Internet, donde la mayoría de ataques se realizan mediante phishing o ingeniería social.



Lee el siguiente artículo del blog de seguridad SecurityArtwork, donde se explica la importancia del factor humano en la prevención de incidentes de seguridad en las organizaciones:

<a href="https://www.securityartwork.es/2016/04/05/human-firewall/?utm_source=feedburner&utm_medium=email&utm_campaign=Feed%253A+SecurityArtWork+%2528Security+Art+Work%2529" target="_blank">El cortafuegos humano</a>
