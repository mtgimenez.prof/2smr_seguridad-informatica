Clasificación De La Seguridad
=============================
Entendiendo la seguridad informática como la disciplina que se ocupa de diseñar las normas, procedimientos, métodos y técnicas destinados a conseguir un sistema de información seguro y confiable, se pueden hacer diversas clasificaciones de la seguridad informática en función de distintos criterios:

1. **Según el ACTIVO a proteger**
      * Física
      * Lógica

2. **Dependiendo del MOMENTO DE ACTUACION**

      * Activa
      * Pasiva

Conocer esta clasificación nos ayudará, a lo largo de todo el módulo, a comprender mejor el objetivo que perseguimos con cada una de las herramientas con las que vamos a trabajar.

## Fisica
La seguridad física es aquella que trata de proteger el **hardware** de las siguientes amenazas:

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;margin:0px auto;}
.tg td{border-color:grey;border-style:dotted;border-width:1px;
  overflow:hidden; word-break:normal;}
.tg th{border-color:grey;border-style:dotted;border-width:1px;font-weight: bold;overflow:hidden;padding:1px 5px;word-break:normal; }
.tg .tg-0pky{border-color:inherit;text-align:left;vertical-align:top}
</style>
<table class="tg">
<thead>
  <tr>
    <th class="tg-0pky">Amenazas</th>
    <th class="tg-0pky">Mecanismos de defensa
</thead>

  </tr>
</thead>
<tbody>
  <tr>
    <td class="tg-0pky">Incendios y humo	</td>
    <td class="tg-0pky">Extintores. Detectores de humo.
</td>
  </tr>
  <tr>
    <td class="tg-0pky">Inundaciones y humedad</td>
    <td class="tg-0pky">Impermeabilización. Correcta ubicación de CPD</td>

  </tr>
  <tr>
    <td class="tg-0pky">Robos</td>
    <td class="tg-0pky">Control de acceso. Sistemas biométricos
</td>
  </tr>
  <tr>
    <td class="tg-0pky">Ruido eléctrico	</td>
    <td class="tg-0pky">Correcta ubicación de CPD <a href="https://www.xataka.com/pro/centros-procesamiento-datos-extranos-mundo-antigua-base-submarinos-nazi-al-fondo-mar" target="_blank">(Ejemplos).</a>
</td>
  </tr>
  <tr>
    <td class="tg-0pky">Terremotos</td>
    <td class="tg-0pky">Construcción acondicionada en lugares propensos (Japón)
</td>
  </tr>
  <tr>
    <td class="tg-0pky">Temperaturas extremas	</td>
    <td class="tg-0pky">Acondicionadores de aire (10~32ºC)
</td>
  </tr>
  <tr>
    <td class="tg-0pky">Electricidad</td>
    <td class="tg-0pky">SAI. Filtros para evitar picos de tensión. Depende de nuestra ubicación
</td>
  </tr>
</tbody>
</table>

## Lógica
La seguridad lógica complementa a la seguridad física, protegiendo el **software** de los equipos informáticos, es decir, las aplicaciones y los datos de usuario. Las principales amenazas y mecanismos de defensa son:  

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;margin:0px auto;}
.tg td{border-color:grey;border-style:dotted;border-width:1px;
  overflow:hidden; word-break:normal;}
.tg th{border-color:grey;border-style:dotted;border-width:1px;font-weight: bold;overflow:hidden;padding:1px 5px;word-break:normal; }
.tg .tg-0pky{border-color:inherit;text-align:left;vertical-align:top}
</style>
<table class="tg">
<thead>
  <tr>
    <th class="tg-0pky">Amenazas</th>
    <th class="tg-0pky">Mecanismos de defensa
</thead>

  </tr>
</thead>
<tbody>
  <tr>
    <td class="tg-0pky">Robos	</td>
    <td class="tg-0pky">Cifrado de información.
</td>
  </tr>
  <tr>
    <td class="tg-0pky">Pérdida de información	</td>
    <td class="tg-0pky">Sistema de backups. Sistema de ficheros redundantes</td>

  </tr>
  <tr>
    <td class="tg-0pky">Pérdida de integridad de la información</td>
    <td class="tg-0pky">Programas de chequeo/bakcup
</td>
  </tr>
  <tr>
    <td class="tg-0pky">Virus	</td>
    <td class="tg-0pky">Antiviurs. Actualizaciones
</td>
  </tr>
  <tr>
    <td class="tg-0pky">Ataques desde la red</td>
    <td class="tg-0pky">Firewalls. Proxy. Monitorización de red
</td>
  </tr>
  <tr>
    <td class="tg-0pky">Intrusiones	</td>
    <td class="tg-0pky">Contraseñas. Listas de control de acceso. Cifrado de información
</td>
  </tr>
</tbody>
</table>

## Activa
La seguridad activa la podemos definir como el conjunto de medidas que **previenen** e intentan evitar los daños en los sistemas informáticos. Sus principales técnicas son:

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;margin:0px auto;}
.tg td{border-color:grey;border-style:dotted;border-width:1px;
  overflow:hidden; word-break:normal;}
.tg th{border-color:grey;border-style:dotted;border-width:1px;font-weight: bold;overflow:hidden;padding:1px 5px;word-break:normal; }
.tg .tg-0pky{border-color:inherit;text-align:left;vertical-align:top}
</style>
<table class="tg">
<thead>
  <tr>
    <th class="tg-0pky">Técnicas</th>
    <th class="tg-0pky">¿Qué previenen?
</thead>

  </tr>
</thead>
<tbody>
  <tr>
    <td class="tg-0pky">Contraseñas	</td>
    <td class="tg-0pky">Acceso a recursos por parte de personal no autorizado.

</td>
  </tr>
  <tr>
    <td class="tg-0pky">Listas de control de acceso (ACL)		</td>
    <td class="tg-0pky">Acceso a ficheros no autorizados</td>

  </tr>
  <tr>
    <td class="tg-0pky">Encriptación</td>
    <td class="tg-0pky">Información no interpretable por personas no autorizadas
</td>
  </tr>
  <tr>
    <td class="tg-0pky">Firmas y cert. digitales		</td>
    <td class="tg-0pky">Comprobar procedencia y autenticidad de la información
</td>
  </tr>
  <tr>
    <td class="tg-0pky">Sistemas de ficheros con toleraciona a fallos</td>
    <td class="tg-0pky">Fallos de integridad en la información
</td>
  </tr>
  <tr>
    <td class="tg-0pky">Cuotas de disco		</td>
    <td class="tg-0pky">Uso indebido del espacio de disco
</td>
  </tr>
</tbody>
</table>

## Pasiva
La seguridad pasiva complementa a la seguridad activa y se encarga de minimizar los efectos que haya ocasionado algún percance. Alguna de las técnicas más importantes de este tipo de seguridad, y que veremos en este módulo profesional, son las siguientes:

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;margin:0px auto;}
.tg td{border-color:grey;border-style:dotted;border-width:1px;
  overflow:hidden; word-break:normal;}
.tg th{border-color:grey;border-style:dotted;border-width:1px;font-weight: bold;overflow:hidden;padding:1px 5px;word-break:normal; }
.tg .tg-0pky{border-color:inherit;text-align:left;vertical-align:top}
</style>
<table class="tg">
<thead>
  <tr>
    <th class="tg-0pky">Técnicas</th>
    <th class="tg-0pky">¿Cómo minimiza?
</thead>

  </tr>
</thead>
<tbody>
  <tr>
    <td class="tg-0pky">Conjunto de discos redundantes	</td>
    <td class="tg-0pky">Restaurar información no consistente (sin efectos para usuari@s)


</td>
  </tr>
  <tr>
    <td class="tg-0pky">SAI		</td>
    <td class="tg-0pky">Permite un apagado programado
</td>

  </tr>
  <tr>
    <td class="tg-0pky">Backups</td>
    <td class="tg-0pky">Recuperación del sistema más o menos completa ante un robo o pérdida generalizado
</td>

</tbody>
</table>

___
> **¿SABRÍAS?...**
Poner un ejemplo de herramienta para cada uno de los tipo de Seguridad Informática que hemos comentado.
___
