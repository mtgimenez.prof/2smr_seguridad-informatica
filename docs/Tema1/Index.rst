Fundamentos de la Seguridad Informática
=======================================
El espectacular auge de Internet y de los servicios telemáticos ha hecho que los ordenadores y las redes se conviertan en un elemento cotidiano en nuestras casas y en un instrumento imprescindible en las tareas de las empresas.
Las empresas, sea cual sea su tamaño, disponen de equipos conectados a Internet que les ayudan en sus procesos productivos. Cualquier fallo en los mismos puede suponer una gran pérdida económica ocasionada por el parón producido, de modo que es muy importante asegurar un correcto funcionamiento de los sistemas y redes informáticas.
Con unas buenas políticas de seguridad, tanto físicas como lógicas, conseguiremos que nuestros sistemas sean menos vulnerables a las distintas amenazas.
Tenemos que intentar lograr un nivel de seguridad razonable y estar preparados para que, **cuando se produzcan los ataques**, los daños puedan ser evitados o en caso contrario haber sido lo suficientemente precavidos para realizar las copias de seguridad.

Los **objetivos principales** de este tema introductorio son los siguientes:


#. Valorar la necesidad de aplicar medidas de seguridad a los sistemas informáticos
#. Conocer los objetivos que persigue la seguridad informática
#. Clasificar los diferentes tipos de seguridad que existen.
#. Clasificar los principales mecanismos de seguridad de acuerdo al tipo al que pertenecen.
#. Conocer los principales tipos de ataques y atacantes.
#. Conocer estándares y legislación que afectan a la seguridad informática.

**Algunas cosas que se han dicho sobre la SI**

Personalidades destacadas del mundo de la Seguridad Informática han dicho algunas cosas muy interesantes a lo largo de los años:

    "Las organizaciones gastan millones de dólares en firewalls y dispositivos de seguridad, pero tiran el dinero porque ninguna de estas medidas cubre el eslabón más débil de la cadena de seguridad: la gente que usa y administra los ordenadores."

    -- `Kevin Mitnick <https://es.wikipedia.org/wiki/Kevin_Mitnick>`_

    "Si piensas que la tecnología puede solucionar tus problemas de seguridad, está claro que ni entiendes los problemas ni entiendes la tecnología"

    "Me preguntan con regularidad lo que el usuario medio de Internet puede hacer para asegurar su seguridad. Mi primera respuesta suele ser 'Nada, estás jodido'."

    -- `Bruce Schneier <https://es.wikipedia.org/wiki/Bruce_Schneier>`_

      "El único sistema verdaderamente seguro es el que está apagado y desenchufado encerrado en una caja fuerte de titanio revestido, enterrado en un búnker de hormigón, y rodeado por gas nervioso y guardias armados muy bien remunerados. Incluso entonces, no apostaría mi vida por él."

    -- `Gene Spafford <https://en.wikipedia.org/wiki/Gene_Spafford>`_

Contenidos
----------

Para ello vamos a analizar los siguientes apartados, los cuales serán **básicos** para poder trabajar el resto del curso en los distintos servicios que aprenderemos a configurar.

.. toctree::
   :maxdepth: 2

   Objetivos
   Clasificacion
   Amenazas
   Ciclo
   Legislacion
   Practicas
