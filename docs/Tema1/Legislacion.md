Legislación Sobre Seguridad Informática
=======================================================
Muy a menudo, en nuestra vida diaria, nuestros datos personales son solicitados para realizar diversos trámites en empresas o en organismos tanto públicos como privados. El objetivo de la ley es garantizar y proteger los derechos fundamentales y, especialmente, la intimidad de las personas físicas en relación con sus datos personales. Es decir, especifica para qué se pueden usar, cómo debe ser el procedimiento de recogida que se debe aplicar y los derechos que tienen las personas a las que se refieren, entre otros aspectos. Toda esta legislación depende de la normativa que llegue de la Comisión Europea la cual publicó en 2018 una ley, el **Reglamento General de Protección de Datos (RGPD - GDRP en inglés)**  que introdujo importante modificaciones en la materia:

<div style="text-align:center;">
<a title="Dooffy / CC0" href="https://commons.wikimedia.org/wiki/File:Gdpr_Europe.jpg"><img width="250" alt="Gdpr Europe" src="https://upload.wikimedia.org/wikipedia/commons/thumb/1/15/Gdpr_Europe.jpg/512px-Gdpr_Europe.jpg"></a>
</div></br>


En España, la gestión y la supervisión de su cumplimiento está a cargo de la  [**Agencia Española de Protección de Datos**](https://www.aepd.es/es), la cual tuvo que introducir importantes cambios para poder adaptarse a la nueva norma europea, transformando la antigua LOPD en la **LOPDGDD Ley Orgánica de Protección de Datos y Garantía de Derechos Digitales**.
A partir de ese momento tanto empresas como instituciones tuvieron que hacer un esfuerzo para seguir cumpliendo la legalidad, que de manera muy resumida puede verse así:

<div style="text-align:center;">
<a title="Cambios LOPDGDD" href="https://www.setemcat.com/novedades-de-lopdgdd-3-2018/"><img width="500" alt="Gdpr Europe" src="https://ceeivalencia.emprenemjunts.es/fotos/61910_foto.png"></a>
</div></br>

Algunas de estas novedades las podemos ver en cuanto navegamos por Internet, por ejemplo con la **nueva gestión e información de las cookies** que usan los sitios web para trabajar, o en otras circunstancias de nuestro día a día.
Podemos ver como nos condicionan estas normas de manera más habitual de lo que pensamos. Por ejemplo:

![](img/actualizacionCondicionesGoogle.png "Condiciones Google")

___
> **¿SABRÍAS?...**
* ¿Explicar de una manera breve estas novedades en la normativa de protección de datos?
    1. La nueva figura del DPO (Delegado de protección de datos).
    2. Cumplir la política de cookies
    3. Conjunto de derechos incluidos en la normativa.
* ¿Imaginar una situación laboral en la que tengas que ajustar y adaptar tus tareas para cumplir estas normas?
___
