Conectividad
============
Muchos de los elementos de seguridad que podemos usar o implementar, dependen en gran medida de la manera en la que los equipos estén conectados
(**FÍSICAMENTE o a nivel LÓGICO**), sea por el medio que sea (red cableada, inalámbrica...).

A este esquema se le conoce como `TOPOLOGÍA de red <https://es.wikipedia.org/wiki/Topolog%C3%ADa_de_red>`_. Cada una tiene sus ventajas e inconvenientes, en aspectos como:

    * Rendimiento
    * **Seguridad**
    * Coste
    * Disponibilidad

.. raw:: html

    <div style="text-align:center;">
    <a title="Yearofthedragon, CC BY-SA 3.0 &lt;http://creativecommons.org/licenses/by-sa/3.0/&gt;, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Topolog%C3%ADa_de_red.png"><img style="margin-left:auto; margin-right:auto;" width="300" alt="Topología de red" src="https://upload.wikimedia.org/wikipedia/commons/thumb/4/4a/Topolog%C3%ADa_de_red.png/512px-Topolog%C3%ADa_de_red.png"></a>
    </div>

Además de la topología de red, la cual la podemos utilizar para separar los entornos a nuestra voluntad, tenemos más alternativas para poder configurar el acceso y la seguridad
de nuestra LAN, por ejemplo:

    1. **Direccionamiento IP**: con la máscara de subred (`Subnetting <https://www.ionos.es/digitalguide/servidores/know-how/subnetting-como-funcionan-las-subredes/>`_)
    2. **VLAN**: Acrónimo de virtual LAN (`red de área local virtual <https://es.wikipedia.org/wiki/VLAN>`_), es un método para crear redes lógicas independientes dentro de una misma red física.(Wikipedia)
    3. **Seguridad de puertos**: Permite asegurar qué MAC se pueden conectar a los puertos de nuestros **switches**, rechazando aquellas MAC que no reconozcamos.
    4. **VPN**: Virtual Private Network. Acceso a la red local desde el exterior.


.. warning::

        A la hora de configurar equipamiento de red debes tener algunas cuestiones en cuenta, por ejemplo:
            * Si te encuentras con algún dispositivo de red (en este caso switches) con acceso mediante contraseña y no la conoces, puedes resetear el dispositivo a sus valores
              iniciales. **La manera concreta de hacerlo DEPENDERÁ DE LA MARCA Y MODELO del fabricante** (`en el siguiente video <https://vimeo.com/501666206>`_ para un switch Cisco 1800 y en `en el siguiente tutorial <https://supertechman.com.au/restore-cisco-2960-series-switch-to-factory-default/>`_ para un Catalyst 29xx).
            * La primera medida de seguridad es configurar una contraseña de acceso al dispositivo y de acceso al modo de administración (`Priviligeled EXEC en CISCO <https://www.cisco.com/E-Learning/bulk/public/tac/cim/cib/using_cisco_ios_software/02_cisco_ios_hierarchy.htm>`_).
              Puedes ver como realizar esto `en el siguiente enlace <https://www.youtube.com/watch?v=yKTTiicT5Co>`_, en el que también se configura una dirección IP para el switch para poder administrarlo remotamente.


VLAN
-----

Además de poder realizar la configuración de VLAN en switch real, existen herramientas de simulación de redes, como
`GNS3 <https://www.gns3.com/>`_ o `PacketTracer <https://www.netacad.com/es/courses/packet-tracer>`_ que nos permiten practicar e
incluso crear escenarios más complejos que los disponibles con el equipamiento de red 'real' del que podamos disponer.

Para configurar VLAN en PacketTracer:

    1. **Switches** con las VLAN creadas en su configuración.
    2. **Puertos etiquetados**: Con el ID de la VLAN.
    3. **Clientes conectados**. Distintas configuraciones.
        * Ips estáticas: En la misma red IP.
        * DHCP: Comprobar difusiones.

.. raw:: html

              <iframe width="250" style="display:block; margin-left:auto; margin-right:auto;" src="https://www.youtube.com/embed/LEt0wEJpeoc" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


.. warning::

        Prueba a realizar una configuración VLAN con PacketTracer **utilizando la linea de comandos(CLI) del switch**. Siempre es más recomendable realizar las configuraciones con la CLI que con la GUI. Después puedes
        conectar una MV Windows con Putty a un Switch real e intentar las mismas instrucciones.


Seguridad de puertos
--------------------
La seguridad de puertos se puede configurar para permitir una o más direcciones MAC. Si se configura un puerto como seguro y se alcanza la cantidad máxima de direcciones MAC, cualquier intento adicional de conexión de las direcciones MAC desconocidas genera una violación de seguridad.

.. image:: img/seguridadpuertos.png
                :width: 200 px
                :alt: Diagrama Internet(LAN/WAN)
                :align: center

En CISCO, por ejemplo(`manual en la web <https://ccnadesdecero.es/seguridad-switches-ssh-puertos/#2_Seguridad_de_Puertos_de_Switch>`_), tenemos 3 tipos de direcciones MAC seguras.

1. **MAC seguras estáticas**: Se configuran manualmente en un puerto mediante el comando *switchport port-security mac-address dirección-mac**. Se olvidan al reiniciar/apagar el switch.
2. **MAC seguras dinámicas**: Detectadas dinámicamente y se almacenan solamente en la tabla de direcciones. Se eliminan cuando el switch se reinicia/apaga.
3. **MAC seguras persistentes**: Pueden detectarse de forma dinámica o manual, y que después se almacenan en la configuración del switch. No se eliminan cuando se reinicia/apaga el switch.

.. raw:: html

              <iframe width="250" style="display:block; margin-left:auto; margin-right:auto;" src="https://www.youtube.com/embed/m3TULPwxJzM" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


VPN
----

Una red privada virtual en inglés: Virtual Private Network (VPN), es una tecnología de red de computadoras que permite una extensión segura de la red de área local (LAN) sobre una red pública como Internet.

.. image:: img/vpn.png
                :width: 300 px
                :alt: Diagrama Internet(LAN/WAN)
                :align: center

.. raw:: html

              </br><iframe width="250" style="display:block; margin-left:auto; margin-right:auto;" src="https://www.youtube.com/embed/IzxC6UPilqQ" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


.. raw:: html

    </br>
    <div style="text-align: justify; color: orange; background-color: #e0e0e0; border-radius: 25px; padding-top: 20px;padding-right: 30px;padding-bottom: 20px; padding-left: 30px;">
    <u><b>PRÁCTICA 1</b></u></br>
    Realiza la práctica 1 del Tema 5 del aula virtual. Configuración de la conectividad de nuestro switch.
    </div>
