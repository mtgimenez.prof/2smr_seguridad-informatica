Seguridad en Redes
===================
En este tema importante tema abordaremos distintas cuestiones relativas a la **seguridad lógica** que podemos implementar en los **dispositivos conectados a una red**, desde
distintas perspectivas. Recordemos que Internet(**WAN**) no es otra cosa que un conjunto de redes locales(**LAN**) interconectadas entre sí.

    **INTERNET = INTERCONNECTED NETWORK**

.. image:: img/internet.png
                :width: 300 px
                :alt: Diagrama Internet(LAN/WAN)
                :align: center

En cada uno de los apartados veremos distintas herramientas y metodologías que nos permitirán mejorar la seguridad de nuestro Sistema informático **conectado a una red**.

.. toctree::
   :maxdepth: 2

   conectividad
   herramientas
   deteccion
   inalambricas
   ataques
