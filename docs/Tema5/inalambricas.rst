Redes inalámbricas
===================
El término red inalámbrica (Wireless network en inglés) se utiliza para designar la conexión de nodos sin necesidad de una conexión física (cable RJ45 normalmente), ésta se da por medio de ondas electromagnéticas.
Tiene una desventaja considerable ya que para este tipo de red se debe de tener una seguridad mucho mas exigente y robusta que en las redes cableadas.

.. image:: img/ventajasdesventajaswifi.png
        :width: 600 px
        :alt: Wifi vs Cable
        :align: center

**MEDIOS DE TRANSMISIÓN**

Según el rango de frecuencias utilizado para transmitir, dependiendo de las necesidades de la red inalámbrica, el medio de transmisión pueden ser:

* Ondas de radio.
* Microondas terrestres
* Microondas por satélite
* Infrarrojos

**TIPOS DE CONEXIÓN**

Existen varios tipos de conexiones inalámbricas:

* Bluetooth

.. image:: img/Bluetoothversions.png
        :width: 300 px
        :alt: Comparativa Bluetooth
        :align: center

* Wifi

.. image:: img/Wifiversions.png
        :width: 500 px
        :alt: Wifi vs Cable
        :align: center

* 3G/4G/5G

.. image:: img/comparativa4g5g.png
        :width: 400 px
        :alt: Comparativa 4g 5g
        :align: center

**DISPOSITIVOS DE CONEXIÓN**

Existen varios dispositivos de interconexión asociados específicamente a las redes inalámbricas:

* Router inalámbrico
* Puntos de acceso
* Repetidores inalámbricos

    .. Warning::

          ¿Sabrías distinguir entre las características principales de estos dispositivos?¿De qué se encarga cada uno?¿Qué funcionalidad ofrecen?

Configuración de la seguridad
------------------------------

Lo que habitualmente queremos es controlar quien se conecta a nuestra red. Para ello podemos aplicar varias medidas de seguridad, que se pueden agrupar según el nivel OSI en el que aplican:


.. image:: img/pilaosi2.png
        :width: 200 px
        :alt: Comparativa 4g 5g
        :align: center

1. **Nivel de enlace:** Variedad de medidas que podemos tomar para conseguir controlar el acceso:

        * A través de una contraseña común para todos los clientes.
        * A través de una característica del cliente, como por ejemplo la dirección MAC o un nombre de usuario y contraseña.

2. **Nivel físico:** Controlar la señal producida por los puntos de acceso,etc... Utilizando antenas podemos conseguir que la señal salga lo menos posible de los límites deseados.

En el **NIVEL DE ENLACE** tenemos algunos protocolos de seguridad, cada uno con diferentes características:

.. image:: img/comparativaWifiSec.png
        :width: 400 px
        :alt: Comparativa seguridad wifi
        :align: center


Existen multitud de recursos en la web acerca de este tema. Tan importante en el campo de la seguridad informática actual. Una de las webs que más nos puede interesar
es la del `Instituo Nacional de la Ciberseguridad(INCIBE) <https://www.incibe.es/>`_. Como ejemplo aquí tienes un completo video en el que se muestran los peligros de las redes WIFI:

.. raw:: html

        <div style="text-align:center;">
        <iframe width="250" style="display:block; margin-left:auto; margin-right:auto;" src="https://www.youtube.com/embed/D6XX3RTW5BQ" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
        </br>


.. Warning::

          Un ejercicio interesante para comprobar la importancia de configurar correctamente la seguridad en redes inalámbricas consiste en intentar averiguar la contraseña de una red Wifi. **Accede al aula virtual e intenta realizar el ejercicio
          relacionado con las redes WiFi.**
