Ejemplos de ataques
===================

Existen muchos tipos de ataques informáticos vinculados al uso de las redes, entre otros podemos encontrar:

    1. DoS (Denegación de servicio)
    2. ARP Spoofing
    3. Inyección SQL
    4. Escaneo de puertos
    5. XSS o Cross Site Scripting
    6. Man-In-The-Middle
    7. Ataques de ingeniería social



Puedes encontrar más información en la web, por ejemplo:

  * https://ciberseguridad.blog/25-tipos-de-ataques-informaticos-y-como-prevenirlos/
  * https://www.iebschool.com/blog/ciberseguridad-ataques-tecnologia/


.. Warning::

        En la sección **Ejemplos de ataque** del aula virtual puedes encontrar varias guías donde practicar estas técnicas y comprobar lo peligrosas que pueden resultar para
        el normal funcionamiento de tu sistema informático.
