Herramientas de seguridad
=========================
Hay dos elementos imprescindibles que han combinarse en una configuración de red actual, en la que la seguridad sea tenida en cuenta, que son los **cortafuegos(Firewall) y los proxys(HW/SW)**.
Estos elementos pueden organizarse de distinta manera, en función del tipo de red en el que nos encontremos:

    * Si se trata de una red empresarial:

        .. image:: img/redempresarial.png
                        :width: 300 px
                        :alt: Red empresarial
                        :align: center

      .. warning::

        ¿Sabes lo que es una DMZ?¿Qué tipo de funciones puede realizar un proxy?

    * En caso de tratarse de una red local más reducida.

        .. image:: img/reddomestica.png
                    :width: 300 px
                    :alt: Red doméstica
                    :align: center

Como principal diferencia entre los dos elementos, se podría decir que:
  * **Un Firewall actúa como barrera.**
  * **Un proxy actúa como intermediario.**
  * Además los firewall trabajan a nivel 3/4 de la pila OSI, mientras que el proxy trabaja en niveles superiores(aplicación), con lo que tienen capacidades distintas.

.. image:: img/pilaosi.png
            :width: 200 px
            :alt: Red doméstica
            :align: center

Firewall
---------

.. raw:: html

    <p>
    Un cortafuegos(firewall en inglés) es una parte de una red que está diseñada para bloquear el acceso no autorizado, permitiendo al mismo tiempo comunicaciones autorizadas.
    Se trata de un dispositivo o conjunto de dispositivos configurados para permitir, limitar, cifrar, descifrar, el tráfico entre los diferentes ámbitos de red.
    Las principales funciones de un firewall<sup id="fnref:note1"><a class="footnote-ref" href="#fn:note1" role="doc-noteref">1</a></sup>, son:
    </p>

    <div style="text-align:center;">
    <a title="Bruno Pedrozo, CC BY-SA 3.0 &lt;https://creativecommons.org/licenses/by-sa/3.0&gt;, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Firewall.png"><img width="256" alt="Firewall" src="https://upload.wikimedia.org/wikipedia/commons/5/5b/Firewall.png"></a>
    </div>

* Crear una barrera que permita o bloquee intentos para acceder a la información en su equipo.
* Evitar usuarios no autorizados accedan a los equipos y las redes de la organización que se conectan a Internet.
* Supervisar la comunicación entre equipos y otros equipos en Internet.
* Visualizar y bloquear aplicaciones que puedan generar riesgo
* Advertir de intentos de conexión desde otros equipos.
* Advertir ir de intentos de conexión mediante las aplicaciones en su equipo que se conectan a otros equipos.
* Detectar aplicaciones y actualizar rutas para añadir futuras fuentes de información
* Hacer frente a los cambios en las amenazas para la seguridad

**WINDOWS**

Los SO Windows integran en su instalación un buen firewall que nos ofrecen una funcionalidad más que suficiente(puedes ver un detalle de su funcionamiento en el `siguiente artículo <https://www.redeszone.net/tutoriales/seguridad/configuracion-firewall-windows-10/>`_).
Además de esta opción existen multitud de programas externos (muchos gratuitos) que podemos instalar.

.. image:: img/firewallWindows.png
            :width: 300 px
            :alt: Red doméstica
            :align: center

.. raw:: html

              </br>
              <iframe width="250" style="display:block; margin-left:auto; margin-right:auto;" src="https://www.youtube.com/embed/xN3LgCJpWz0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

**LINUX**

En todas las distribuciones de Linux, el cortafuegos usa de una de las herramientas de red mas potentes que ofrecen este tipo de SO, **IPTABLES**:
    * Es un sistema de firewall INTEGRADO EN EL KERNEL de linux.
    * Un firewall de iptables no es como un servidor que lo iniciamos o detenemos o que se pueda caer por un error de programación, iptables ES PARTE DEL SISTEMA OPERATIVO.
    * Realmente lo que se hace es aplicar reglas. Para ello se ejecuta el comando iptables, con el que añadimos, borramos, o creamos reglas. Por ello un firewall de iptables no es sino un simple script de shell en el que se van ejecutando las reglas de firewall.
    * Existen programas que nos permiten interactuar con IPTables de manera más visual, facilitándonos el trabajo (P.Ej. : `UFW/GUFW <https://es.wikipedia.org/wiki/Uncomplicated_Firewall>`_)

.. raw:: html

              <div style="text-align:center;">
              <a title="http://gufw.tuxfamily.org, GPL &lt;http://www.gnu.org/licenses/gpl.html&gt;, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Gufw_10.04.4.png"><img width="256" alt="Gufw 10.04.4" src="https://upload.wikimedia.org/wikipedia/commons/b/ba/Gufw_10.04.4.png"></a>
              </br>
              <iframe width="250" style="display:block; margin-left:auto; margin-right:auto;" src="https://www.youtube.com/embed/mizw1X6OecE" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
              </br></br>
              <div style="text-align: justify; color: orange; background-color: #e0e0e0; border-radius: 25px; padding-top: 20px;padding-right: 30px;padding-bottom: 20px; padding-left: 30px;">
              <u><b>PRÁCTICA 2</b></u></br>
              Práctica 2 del Tema 5 del aula virtual. Configuración del Firewall.
              </div>

Proxy
-----

.. raw:: html

    <p>
    Un proxy, o servidor proxy, en una red informática, es un programa o dispositivo, que hace de intermediario en las peticiones de recursos que realiza un cliente a otro servidor (C).
    Por ejemplo, si una hipotética máquina A solicita un recurso a C, lo hará mediante una petición a B, que a su vez trasladará la petición a C; de esta forma C no sabrá que la petición
    procedió originalmente de A. Esta situación estratégica de punto intermedio le permite ofrecer diversas funcionalidades<sup id="fnref:note2"><a class="footnote-ref" href="#fn:note2" role="doc-noteref">2</a></sup>:
    </p>

* Control de acceso.
* Registro del tráfico,
* Restricción a determinados tipos de tráfico
* Cacheado de contenidos estáticos, mejora de rendimiento
* **Anonimato de la comunicación**.
* **Redirección del tráfico de salida de la red**
* Distribución de carga entre servidores.

.. image:: img/proxy.png
                :width: 300 px
                :alt: Diagrama Proxy
                :align: center

El proxy puede configurarse en los clientes:

1. En el navegador (p.e Firefox)
    .. image:: img/proxynavegador.png
                :width: 300 px
                :alt: Configuración proxy en navegador
                :align: center
2. En el sistema operativo (Se configura para todo el sistema, esta es la opción de Chrome/Explorer/Edge..)
    .. image:: img/proxyso.png
                :width: 300 px
                :alt: Configuración proxy en sistema operativo
                :align: center
3. `Proxy transparente <https://www.linuxparty.es/index.php/57-seguridad/508-linux-instalar-un-proxy-transparente-con-squid/>`_, si queremos 'obligar' el paso por nuestro proxy sin necesidad de configurar SO o navegador.

.. Warning::

        También existe la posibilidad de realizar un script de **auto-configuración** de nuestro proxy(`proxy auto-config (PAC) <https://en.wikipedia.org/wiki/Proxy_auto-config>`_).
        Con esta opción podemos establecer diferentes proxys para nuestros clientes en función de nuestras necesidades.

Tenemos algunos ejemplos interesantes de programas para configurar nuestro proxy:

    * Wingate | CCProxy en Windows
    * Squid en Linux.

**CONFIGURACIÓN SQUID**

Toda la configuración de Squid se realiza en el fichero **/etc/squid3/squid.conf**. Debemos entender los elementos que básicamente haremos dos cosas:

    1. **ACL** → Listas de control de acceso. Incluirán palabras, url, ip, nombres de usuario..
    2. **Reglas** → Se aplicarán a las ACL para permitir o denegar la navegación. Podemos incluso restringir el tráfico por horas/días...

        .. image:: img/ejemploSquid.png
                :width: 400 px
                :alt: Ejemplo squid conf
                :align: center

La mejor manera de aprender es crea distintos casos, intentando utilizar las distintas posibilidades que ofrece Squid. Comienza por escenarios más sencillos, y ves aumentando la complejidad.
**Recuerda que debes configurar tus clientes con alguna de las opciones vistas anteriormente**

.. raw:: html

              <div style="text-align:center;">
              <iframe width="250" style="display:block; margin-left:auto; margin-right:auto;" src="https://www.youtube.com/embed/zXusMCM6p_k" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
              </br></br>
              <div style="text-align: justify; color: orange; background-color: #e0e0e0; border-radius: 25px; padding-top: 20px;padding-right: 30px;padding-bottom: 20px; padding-left: 30px;">
              <u><b>PRÁCTICA 3</b></u></br>
              Práctica 3 del Tema 5 del aula virtual. Configuración de un servidor proxy en Linux.
              </div>

.. Warning::

    Dado que el fichero */etc/squid/squid.conf* es muy extenso, y para conseguir una gestión más sencilla de vuestra configuración de Squid se
    recomiendo ubicar vuestras configuraciones en ficheros creados por vosotr@s mism@s en la carpeta **/etc/squid/conf.d** con la extensión **.conf**. Por
    ejemplo algo como la siguiente imagen:

    .. image:: img/ejemploSquidSeparado.png
            :width: 300 px
            :alt: Ejemplo squid conf
            :align: center

.. raw:: html

   </br>
   <div class="footnotes">
       <hr />
       <ol>
           <li class="footnote" id="fn:note1">
               <p>
                   <b>Fuente:</b> <a href="https://idgrup.com/firewall-que-es-y-como-funciona/" target="_blank">https://idgrup.com/firewall-que-es-y-como-funciona/</a> <a class="footnote-backref" rev="footnote" href="#fnref:note1">&#8617;</a>
               </p>
           </li>
           <li class="footnote" id="fn:note2">
               <p>
                   <b>Fuente:</b><a href="https://www.welivesecurity.com/la-es/2020/01/02/que-es-proxy-para-que-sirve/" target="_blank">https://www.welivesecurity.com/la-es/2020/01/02/que-es-proxy-para-que-sirve/</a>  <a class="footnote-backref" rev="footnote" href="#fnref:note2">&#8617;</a>
               </p>
           </li>
       </ol>
   </div>
