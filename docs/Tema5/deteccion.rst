Sistemas de detección
======================
Detectar posibles intrusiones es una de las funciones más importantes de la seguridad en una red informática, en este sentido tenemos
varias funciones a realizar:

    1. Registro (log) de los accesos hacia|desde nuestra red: con herramientas como **cortafuegos o proxy**.
    2. Revisión de los paquetes de red mediante una herramienta de análisis: Software de sniffing **tipo Wireshark** sería un ejemplo.
    3. **Análisis de nuestra red (IPs y puertos)** en busca de equipos y servicios: Un ejemplo de herramienta muy potente sería `Nmap <https://nmap.org/>`_.
    4. Detección de intrusiones para saber distinguir situaciones de ataque a nuestro sistema. Estos sistemas suelen conocerse como
       **Sistemas de Detección de Intrusos (o IDS en inglés)**: Uno de los mas populares es `Snort <https://www.snort.org/>`_.

En este apartado vamos a comenzar a conocer Herramientas relacionadas con los puntos 3 y 4.

**ANÁLISIS DE RED CON NMAP**

Podemos encontrar multitud de recursos en la web para poder escanear equipos o redes y comprobar los puertos que tiene activos(esto significa
que en cada uno de ellos debe haber un servicio en ejecución). De esta manera nos aseguramos que no hay **puertos abiertos que no conocemos**
y que suponen un agujero de seguridad:

      .. code-block:: shell-session

           $ nmap -sV 127.0.0.1

            Starting Nmap 7.60 ( https://nmap.org ) at 2021-02-21 21:41 CET
            Nmap scan report for localhost (127.0.0.1)
            Host is up (0.00019s latency).
            Not shown: 995 closed ports
            PORT      STATE SERVICE         VERSION
            111/tcp   open  rpcbind         2-4 (RPC #100000)
            631/tcp   open  ipp             CUPS 2.2
            902/tcp   open  ssl/vmware-auth VMware Authentication Daemon 1.10 (Uses VNC, SOAP)
            2049/tcp  open  nfs_acl         3 (RPC #100227)
            56738/tcp open  unknown

Existen alternativas gráficas para utilizar nMap y poder perfeccionar nuestras técnicas de análisis de la red.Una de las más populares es
`Zenmap <https://nmap.org/zenmap/>`_.

.. image:: img/zenmap.jpg
        :width: 400 px
        :alt: Zenmap GUI for nmap
        :align: center

Dado que este tipo de herramientas también puedes utilizarse con fines maliciosos, **proteger tu red de posibles escaneos con Nmap** también es una de las tareas más importantes que puedes configurar
en tu firewall.

.. Warning::

      Una práctica interesante sería conocer los equipos de tu red que están ofreciendo servicios en tu red local. Ejecuta la instrucción
      correspondiente y analiza los resultados. Si lo prefieres puedes usar una alternativa gráfica, instalándola en una Máquina Virtual. **¿Que máquina ofrece más puertos abiertos en
      la red del aula?¿Por qué?¿Sabrías identificar que servicios ofrece?**



**DETECCIÓN DE INTRUSIONES CON SNORT**

Hay que distinguir entre un IDS(Sistema de detección de intrusiones) y un sistema de **prevención** tipo firewall o proxy. Los IDS se van a
encargar de **alertar** de determinadas actividades en nuestro sistema informático conectado a la red.

+-----------------------------------------+
| .. figure:: img/Ips-vs-ids.png          |
|   :alt: IDS vs IPS                      |
|                                         |
|   **BoBeni, CC BY-SA 4.0, via           |
|   Wikimedia Commons**.                  |
+-----------------------------------------+

La elección de una correcta ubicación de estas herramientas en nuestra red es fundamental. Se podría ubicar, por ejemplo, en un punto de salida de la red
(ROUTER) para avisarnos de accesos inapropiados.

Una vez correctamente ubicado e instalado, la realización de reglas puede ser algo complejo, aunque nos podemos ayudar con videos como los siguientes:

.. raw:: html

              <div style="text-align:center;">
              <iframe width="250" style="display:block; margin-left:auto; margin-right:auto;" src="https://www.youtube.com/embed/rLY1uPBBuD0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
              </br>
              <div style="text-align:center;">
              <iframe width="250" style="display:block; margin-left:auto; margin-right:auto;" src="https://www.youtube.com/embed/gkzmPdkSUgc" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
              </br>
