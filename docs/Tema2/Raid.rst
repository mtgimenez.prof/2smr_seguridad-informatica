RAID
=====

El acrónimo **RAID** (del inglés Redundant Array of Independent Disks, «conjunto redundante de discos independientes», anteriormente conocido como Redundant Array of Inexpensive Disks, «conjunto redundante de discos baratos») hace referencia a un sistema de almacenamiento que usa múltiples discos duros entre los que se distribuyen o replican los datos, dependiendo de su configuración («nivel»). Podemos encontrarlos a nivel de **hardware** (como en la imagen siguiente) o configurarlos por **software** (que es lo que haremos nosotr@s)

.. image:: img/raid.png
   :width: 300 px
   :alt: NAS Diagram
   :align: center


.. raw:: html

    <p>
      <b>TIPOS DE RAID.</b><sup id="fnref:note1"><a class="footnote-ref" href="#fn:note1" role="doc-noteref">1</a></sup>
    </p>


* **Estándar**:Configuración básica de RAID. Hay 9 tipos, los más usados son 3
    * RAID 0: Conjunto dividido (striped)
    * RAID 1: Conjunto en espejo (mirror)
    * RAID 5: Conjunto dividido con paridad distribuida (parity)
* **Anidados**: Un RAID puede pertenecer a otro RAID
* **Propietarios**: Desarrollados por empresas (€€)



**RAID nivel 0 (RAID0):**

En este nivel los datos se distribuyen equilibradamente entre dos o más discos. Como podemos ver en la figura de abajo los bloques de la unidad A se almacenan de forma alternativa entre los discos 0 y 1 de forma que los bloques impares de la unidad se almacenan en el disco 0 y los bloques pares en el disco 1.

.. image:: img/raid0.png
   :width: 200 px
   :alt: NAS Diagram
   :align: center

* Esta técnica favorece la velocidad debido a que cuando se lee o escribe un dato, si el dato está almacenado en dos discos diferentes, se podrá realizar la operación simultáneamente.
* RAID 0 no incluye ninguna información redundante, por lo que en caso de producirse un fallo en cualquiera de los discos que componen la unidad provocaría la pérdida de información en dicha unidad.

**RAID nivel 1 (RAID1):**

A menudo se conoce también como **espejo o mirror**. Consiste en mantener una copia idéntica de la información de un disco en otro u otros discos.

.. image:: img/raid1.png
   :width: 200 px
   :alt: NAS Diagram
   :align: center

* Si se produjera un fallo en un disco la unidad podría seguir funcionando sobre un solo disco.
* El espacio de la unidad se reduce a la mitad del espacio disponible.

**RAID nivel 5 (RAID5):**

Los bloques de datos que se almacenan en la unidad, y la información redundante de dichos bloques se distribuye cíclicamente entre todos los discos que forman el volumen RAID5. Por ejemplo si aplicamos RAID5 sobre un conjunto de 4 discos los bloques de datos se colocan en tres de los cuatro discos, dejando un hueco libre en cada línea que irá rotando de forma cíclica. En este hueco se colocará un bloque de paridad. Con este sistema, el bloque de paridad se coloca cada vez en un disco.

.. image:: img/raid5.png
   :width: 200 px
   :alt: NAS Diagram
   :align: center

* El bloque de paridad se calcula a partir de los bloques de datos de la misma línea, de forma que el primero será un 1, si hay un número impar de unos en el primer bit de los bloques de datos de la misma línea, y 0 si hay un número par de unos.
* Se necesitan un **mínimo de 3** discos/particiones.
* Se pierde :math:`\frac{1}{N}` de la capacidad, donde *N* es el número de discos/particiones.


**RAID EN WINDOWS**

    **5 tipos de volúmenes dinámicos:**

      * Simples: Es un volumen que utiliza espacio de un solo disco físico.
      * Distribuidos:(Spanned) Se crea ocupando espacio de varios discos sin existir una regla que especifique cómo tienen que almacenarse los datos en los discos.
      * Seccionados(Striped): Corresponde con el nivel 0 de RAID.
      * **Reflejados(Mirrored)**: Corresponde con el nivel 1 de RAID.
                .. raw:: html

                          <iframe width="250" style="display:block; margin-left:auto; margin-right:auto;" src="https://www.youtube.com/embed/asuWZDN7Xpk" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

      * **Raid-5**: Corresponde con el nivel 5 de RAID.
                .. raw:: html

                          <iframe width="250" style="display:block; margin-left:auto; margin-right:auto;" src="https://www.youtube.com/embed/vLio5ixrS_0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

.. raw:: html

    </br>
    <div style="text-align: justify; color: orange; background-color: #e0e0e0; border-radius: 25px; padding-top: 20px;padding-right: 30px;padding-bottom: 20px; padding-left: 30px;">
    <u><b>PRÁCTICA 2</b></u></br>
    Realiza la práctica 2 del Tema 2 del aula virtual, sobre creación de discos RAID en Windows.
    </div>
    </br>


**RAID EN LINUX**

    Linux ofrece un soporte más avanzado para el montaje de RAID. A través de la herramienta **MDADM** nos va a permitir muchas mas cosas que las herramientas de windows. Hay que tener en cuenta algunos conceptos nuevos:

      * **Disco de emergencia (spare disk)** → No forman parte de la sección ACTIVA del RAID, no son utilizables hasta que haya una averia, momento en el que se activa el primer spare. ARREGLAR EL RAID LO ANTES POSIBLE
      * **Disco Averiado (faulty)** → Siguen perteneciendo al RAID.
      * **Intercambio en caliente (Hot Swap)** → Algunas interfaces si (SATA, SCSI) otras no (IDE)

    Linux reconoce los discos RAID como dispositivos de bloques (disco duro, cd…), y puede formarlos tanto con discos enteros como con particiones, además de poder incluir como miembros de RAID a otros RAID (**RAID ANIDADO**).

    **Creación del RAID**

        * Preparar los dispositivos
            * Discos/particiones?
            * Disp. Loopback?
            * Gparted/Linea de comandos(fdisk)
            * Tabla de particiones (mbr/gpt)
        * Instalar MDADM
        * Cargar los módulos RAID correspondientes
            .. code-block:: shell-session

              #cat /proc/mdstat
              #modprobe raid456
        * Crear el RAID
            .. code-block:: shell-session

              #mdadm --create /dev/md0 --level=raid0 --raid-devices=2 /dev/sdb1 /dev/sdc1
              #mdadm --create /dev/md1 --level=raid1 --raid-devices=2 /dev/sdd1 /dev/sde1 –-spare-devices=1 /dev/sdf1
              #mdadm --create /dev/md2 --level=raid5 --raid-devices=3 /dev/sdg1 /dev/sdh1 /dev/sdei1 –-spare-devices=1 /dev/sdj1
        * Formatear el RAID
            .. code-block:: shell-session

              #mkfs -t ext4 /dev/md0
        * Montar la unidad
            .. code-block:: shell-session

              #mount /dev/md0 /mnt/raid



        Si queremos hacer el montaje automático al iniciar el equipo debemos incluir la información correspondiente en el fichero **/etc/fstab**. Recuerda usar los UUID de las unidades de almacenamiento para evitar problemas con la numeración del RAID (en el `siguiente manual <https://wiki.archlinux.org/index.php/Fstab_(Espa%C3%B1ol)>`_ puedes encontrar una explicación más detallada).

    .. image:: img/iconoRaidUbuntu.jpg
      :alt: Raid montado en Ubuntu desktop
      :align: center


    **Gestión del RAID**

          .. image:: img/estadoRaidUbuntu.png
              :width: 400 px
              :alt: Raid montado en Ubuntu desktop
              :align: center

        * Estado del RAID
            .. code-block:: shell-session

              #mdadm --detail /dev/md0
        * Quitar un disco del RAID
            .. code-block:: shell-session

              #mdadm /dev/md0 -r /dev/sde3
        * Añadir disco al RAID
            .. code-block:: shell-session

              #mdadm /dev/md0 -a /dev/sde3
        * Parar/Iniciar la matriz de un RAID
            .. code-block:: shell-session

              #mdadm  --stop|--assemble|--run /dev/md0
        * Limpiar info previa de los participantes RAID
            .. code-block:: shell-session

              #mdadm --zero-superblock /dev/sdb1


    **Simulación de avería**

    MDADM ofrece la funcionalidad suficiente para simular una avería en alguno/s de los dispositivos de nuestro RAID, con el fin de poder comprobar el correcto funcionamiento del proceso de recuperación.

        * Poner en modo fallo algún miembro del RAID

          .. image:: img/falloRaidUbuntu.png
            :width: 400 px
            :alt: Raid montado en Ubuntu desktop
            :align: center

        * Sustituir el dispositivo erróneo.

          .. image:: img/cambioRaidUbuntu.png
            :width: 400 px
            :alt: Raid montado en Ubuntu desktop
            :align: center

    Puedes encontrar más ayuda en el este `completo manual de creación y configuración de RAID en Ubuntu <https://www.guia-ubuntu.com/index.php/Crear_una_Software_RAID>`_.

.. raw:: html

        </br>
        <div style="text-align: justify; color: orange; background-color: #e0e0e0; border-radius: 25px; padding-top: 20px;padding-right: 30px;padding-bottom: 20px; padding-left: 30px;">
        <u><b>PRÁCTICA 3</b></u></br>
        Realiza la práctica 3 del Tema 2 del aula virtual, sobre creación de discos RAID en sistemas LINUX.
        </div>
        </br>


.. raw:: html

   </br>
   <div class="footnotes">
       <hr />
       <ol>
           <li class="footnote" id="fn:note1">
               <p>
                   <b>Fuente:</b> <a href="https://es.wikipedia.org/wiki/RAID" target="_blank">Wikipedia</a> <a class="footnote-backref" rev="footnote" href="#fnref:note1">&#8617;</a>
               </p>
           </li>
       </ol>
   </div>
