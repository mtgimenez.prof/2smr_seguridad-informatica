Recuperación de la información
==============================

En los entornos reales se pueden producir, bien sea por fallos graves en el Hardware (problema eléctrico) o por ataques malintencionados (ransomware), situaciones en las que resultará fundamental tener algún mecanismo que permita minimizar el impacto del problema, dándonos opciones para:

* Restaurar los equipos a un estado operacional lo antes posible.
* Evitar la pérdida masiva de información.
* Cumplir con lo establecido tanto por las leyes como por estándares.


Vamos a repasar algunas de las herramientas que nos ayudarán a 'sobrevivir' en alguna situación como estas. En esta apartado estaríamos hablando de seguridad **PASIVA**, y trataremos elementos como:

.. toctree::
   :maxdepth: 2

   CSeguridad
   Restauracion
   Imagenes
