Imágenes del sistema
==============================
Una imagen de disco es un FICHERO que contiene la estructura y contenidos completos de un DISPOSITIVO O PARTICIÓN. Una imagen de disco usualmente se produce creando una copia completa, SECTOR POR SECTOR del origen(CLONADO).
Algunas herramientas de creación de imágenes de disco omiten el espacio no utilizado del medio de origen, o comprimen el disco que representan para reducir los requisitos de almacenamiento.
Se suelen ofrecer funcionalidades como:

* Exportación a diferentes formatos
* Difusión por red
* Almacenado remoto

.. image:: img/estructuraDisco.png
    :width: 400 px
    :alt: Estructura disco
    :align: center


Ejemplos de aplicaciones:
      * WinISO
      * Alcohol 120%
      * **Clonezilla**
      * Comando DD??
      * ................

.. raw:: html

        <div style="text-align: justify; color: BLUE; background-color: #e0e0e0; border-radius: 25px; padding-top: 20px;padding-right: 30px;padding-bottom: 20px; padding-left: 30px;">
        <u>¿Sabrías?...</u></br>
        Imaginar alguna situación en la que te podría ser útil utilizar el clonado de imágenes.
        </div></br>

Puedes usar Virtualbox para practicar la creación y restauración de imágenes, sin tocar ninguna instalación real.

    .. raw:: html

              <iframe width="250" style="display:block; margin-left:auto; margin-right:auto;" src="https://www.youtube.com/embed/3YyUUgXfYSM" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
