Seguridad Eléctrica
================
Como todo sistema electrónico, un sistema informático se encuentra expuesto a los problemas relacionados con la alimentación eléctrica, entre ellos:

* PICO
* RUIDO ELÉCTRICO
* CORTE DE LUZ
* CAÍDAS DE TENSIÓN
* ?????????

<div style="text-align: center; color: BLUE; background-color: #e0e0e0; border-radius: 25px; padding-top: 20px;padding-right: 30px;padding-bottom: 20px; padding-left: 30px;">
ES UNA DE LAS PRINCIPALES CAUSAS DE FALLOS EN EL HARDWARE
</div></br>

___
> **¿SABRÍAS?...**
¿Definir con tus propias palabras cada uno de los términos anteriores?¿Y encontrar algún otro tipo de fallo eléctrico?
___

Para tratar de minimizar estos problemas, sobre los cuales muchas veces no tenemos una influencia directa ¿?¿?¿, la mejor alternativa es la utilización de un **S**istema de **A**limentación **I**ninterrumpida (**U**ninterrupted **P**ower **S**upply en inglés)

**SAI (UPS)**
Dispositivo que permite dar energía eléctrica constante a una computadora incluso si el suministro principal de energía se ve interrumpido. Para ello utiliza una batería cargada. Estos dispositivos también suelen funcionar como reguladores de voltaje eléctrico. Son muy útiles en la seguridad de cualquier computadora pues impiden la pérdida de datos e incluso la rotura física de algún dispositivo tras un corte eléctrico.

<div style="text-align:center;">
<a href="https://es.wikipedia.org/wiki/Archivo:UPSAPC.jpg#filelinks" target="_blank"><img width="300" alt="UPS Front and rear" src="https://upload.wikimedia.org/wikipedia/commons/b/b0/UPSAPC.jpg"></a>
</div>

</br>
<p>
    <b>Modos de Funcionamiento<sup id="fnref:note1"><a class="footnote-ref" href="#fn:note1" role="doc-noteref">1</a></sup></b>
</p>

1. Standby ú Off-Line
2. Interactivos
3. On Line

**Capacidades**
1. Monitorización
2. Gestión de apagado/encendido

</br>
<div style="text-align: justify; color: orange; background-color: #e0e0e0; border-radius: 25px; padding-top: 20px;padding-right: 30px;padding-bottom: 20px; padding-left: 30px;">
<u><b>PRÁCTICA 1</b></u></br>
Realiza la práctica 1 del Tema 2 del aula virtual.
</div>
</br>
<div class="footnotes">
    <hr />
    <ol>
        <li class="footnote" id="fn:note1">
            <p>
                <b>Fuente:</b> <a href="https://www.rackonline.es/content/que-es-un-sai-y-tipos-de-sai" target="_blank">Qué es un SAI y tipos de  SAI</a> <a class="footnote-backref" rev="footnote" href="#fnref:note1">&#8617;</a>
            </p>
        </li>
    </ol>
</div>
