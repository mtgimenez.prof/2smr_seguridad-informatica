Copias de Seguridad
====================

Las copias de seguridad (**backups**) se realizan con el objetivo de poder recuperar datos tras una eventual pérdida de información. Para mucha gente *"Las copias de seguridad son la salvaguarda básica para proteger la información de la empresa"*.

Dependiendo del tamaño y necesidades de la empresa, los soportes, la frecuencia y los procedimientos para realizar las copias de seguridad pueden ser distintos. El soporte escogido dependerá del sistema de copia seleccionado, de la fiabilidad que sea necesaria y de la inversión que deseemos realizar. Estas tres variables van estrechamente unidas y deben estar en consonancia con la estrategia de nuestra organización.

.. raw:: html

    <p>
      En la implantación de un sistema de copias debemos tener en cuenta al menos las siguientes consideraciones <sup id="fnref:note1"><a class="footnote-ref" href="#fn:note1" role="doc-noteref">1</a></sup>:
    </p>

* Determinar la **información** de la que se va a realizar la copia, así como el **sistema de almacenamiento**.
* Cantidad de versiones que vamos a almacenar de cada elemento guardado, y su periodo de conservación. Esto es lo que se conoce como **política de copias de seguridad**. Para ello se combinarán distintos tipos de backup.
* **Nomenclatura** que se seguirá a la hora de guardar los ficheros de copia de seguridad (dia|mes|año|hora...)

Estas decisiones afectan al funcionamiento general del sistema informático, y en ella influyen as necesidades del negocio y la capacidad de almacenamiento disponible, conformando lo que se puede llamar el **PLAN DE COPIAS DE SEGURIDAD**.

Tipos de backup
----------------

Las copias de seguridad se suelen agrupar en tres tipos básicos, dependiendo de la estrategia elegida para almacenar la información.

* En la **copia total**, se realiza una copia completa y exacta de la  información  original,  independientemente  de  las  copias  realizadas anteriormente.
* En los **backups diferenciales** cada vez que se realiza una copia de seguridad, se copian todos los archivos que ha-yan sido modificados desde la última copia completa
    .. image:: img/csegDiferencial.png
          :width: 400 px
          :alt: backup incremental
          :align: center
* En el caso de los **sistemas de copia incremental**, únicamente se copian los archivos que se hayan añadido o modificado desde la última copia realizada, sea total o incremental.
    .. image:: img/csegIncremental.png
          :width: 400 px
          :alt: backup incremental
          :align: center

Dependiendo de un tipo u otro de backup las necesidades de almacenamiento varían bastante, además el **proceso de restauración** puede ser más complejo. Una de las utilidades que ofrece **LVM** es la generación de **Snapshots** que pueden `guardarse como copia de seguridad <https://www.instructables.com/Snapshots-con-LVM-Usalos-como-backup-para-revertir/>`_


.. raw:: html

    <div style="text-align: center; color: BLUE; background-color: #e0e0e0; border-radius: 25px; padding-top: 20px;padding-right: 30px;padding-bottom: 20px; padding-left: 30px;">
    Es importante poder determinar que copias necesitarías para restaurar la información de un sistema, en función del tipo o tipos de backup que hayas utilizado.
    </div></br>

Copias de Seguridad en Windows
-------------------------------

Podemos utilizar un Software especializado o utilizar las utilidades de backup que incorpora el propio Sistema Operativo.

  **A.- UTILIDADES DEL SISTEMA OPERATIVO**

    Desde hace algunas versiones, Windows incorpora un aplicación para la creación y restauración de copias de seguridad bastante útil (`Manual para Windows 10 <https://support.microsoft.com/es-es/help/4027408/windows-10-backup-and-restore>`_)

    .. raw:: html

        <div style="text-align: justify; color: BLUE; background-color: #e0e0e0; border-radius: 25px; padding-top: 20px;padding-right: 30px;padding-bottom: 20px; padding-left: 30px;">
        <u>¿Sabrías?...</u></br>
        ¿Hacer la copia de seguridad de alguna de tus carpetas en una <b>unidad externa</b> utilizando las herramientas que incluye el SO?
        </div></br>

  **B.- SOFTWARE DE COPIAS DE SEGURIDAD**

    Utilizaremos el SW `EaseUs Todo Backup Free <https://www.easeus.com/backup-software/tb-free.html>`_, herramienta que nos proporciona mayor cantidad de opciones que la vista en el apartado anterior.Cuenta además con una `muy buena documentación <https://www.easeus.com/tutorial/tb-free-user-guide.html>`_.

    Con programas como este podemos establecer un completo **plan de copias de seguridad**.

    .. raw:: html

        <div style="text-align: justify; color: BLUE; background-color: #e0e0e0; border-radius: 25px; padding-top: 20px;padding-right: 30px;padding-bottom: 20px; padding-left: 30px;">
        <u>¿Sabrías?...</u></br>
        ¿Hacer la copia de seguridad de alguna de tus carpetas en una <b>unidad externa</b> utilizando la herramienta que acabamos de comentar?Planifica una política de copias de seguridad en Windows.
        </div></br>


Copias de Seguridad en Linux
-----------------------------
El tratamiento de las copias de seguridad está mejor integrado en el propio SO en Linux, ya que con algunos de los comandos que incorpora podemos configurar perfectamente nuestros backups. Aún así existen algunos programas interesantes.

  **A.- UTILIDADES DEL SISTEMA OPERATIVO**

    Todas estas utilidades las incluyen la gran mayoría de distribuciones Linux por defecto, luego no es necesario instalar ningún Software específico. Podemos usar, por ejemplo, alguna combinación de los siguientes comandos:

    * **Sincronización remota de datos(RSYNC):** Nos permite copiar en remoto y elegir el tipo de copia (incremental..)
    * **Generación de CS comprimida (TAR):** Comprimir la Cseg nos permitirá ahorrar espacio, además de comprobar la integridad de los datos y elegir qué se copia (diferencial, incremental..)
    * **Automatización de las tareas de Backup(CRON):** Programar la ejecución de las distintas copias según nuestro plan de copias de seguridad.

    Con un poco de trabajo por nuestra parte, podemos configurar la copia de seguridad justo con las características deseadas, y entendiendo que se ejecuta en nuestro sistema exactamente, algo que siempre es más seguro que utilizar un programa de terceros.


          .. raw:: html

              <p style="text-align: justify;"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/4/42/Pdf-2127829.png/480px-Pdf-2127829.png" alt="Perfil" width="50" style="vertical-align: middle; float:left;"/>  En el siguiente documento puedes encontrar un manual completo de como realizar un backup programado, incluyendo toda la funcionalidad en un <b>SCRIPT</b>. </br> </br>

          .. image:: img/BackupLinux.pdf
              :width: 400 px
              :alt: Manual Copias de Seguridad en sistemas Linux
              :align: center

    .. raw:: html

        </br>
        <div style="text-align: justify; color: orange; background-color: #e0e0e0; border-radius: 25px; padding-top: 20px;padding-right: 30px;padding-bottom: 20px; padding-left: 30px;">
        <u><b>PRÁCTICA 5</b></u></br>
        Realiza la práctica 5 del Tema 2 del aula virtual, vas a crear y documentar tu plan de backup en Linux.
        </div>

  **B.- SOFTWARE DE COPIAS DE SEGURIDAD**

    No son demasiadas las alternativas que tenemos como SW de usuario con GUI si queremos programar nuestras copias de seguridad en Linux. Una de las que podemos utilizar es `DejaDup <https://wiki.gnome.org/Apps/DejaDup>`_, la cual permite gestionar y restaurar nuestros backup de manera muy sencilla (perdiendo la flexibilidad que tenemos si usamos los comandos vistos anteriormente)

      .. image:: img/dejaDupBackup.png
          :width: 400 px
          :alt: backup incremental
          :align: center

    Para entornos profesionales y de empresa, con necesidades distintas a las de un simple sistema informático doméstico, tenemos muchas alternativas, con las que podemos gestionar backup programados remotos con múltiples sistemas de almacenamiento. Algunos programas realizan estas funciones, necesitando la instalación de BBDD como MySQL, entre todas ellas destaca el sistema `Bacula <https://www.bacula.org/>`_.

      .. image:: img/BaculaBackup.png
          :width: 400 px
          :alt: backup incremental
          :align: center


.. raw:: html

   </br>
   <div class="footnotes">
       <hr />
       <ol>
           <li class="footnote" id="fn:note1">
               <p>
                   <b>Fuente:</b> <a href="https://www.incibe.es/sites/default/files/contenidos/guias/doc/guia_decalogo_ciberseguridad_metad.pdf" target="_blank">Decálogo ciberseguridad empresas del INCIBE (Sección "2.3 Copias de seguridad")</a> <a class="footnote-backref" rev="footnote" href="#fnref:note1">&#8617;</a>
               </p>
           </li>
       </ol>
   </div>
