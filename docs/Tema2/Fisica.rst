Seguridad Física
================
El primer paso para establecer la seguridad de un servidor o un equipo es decidir adecuadamente dónde vamos a instalarlo. Esta decisión resulta vital para el mantenimiento y protección de nuestros sistemas. Los planes de seguridad física se basan en proteger el hardware de los posibles desastres naturales, de incendios, inundaciones, sobrecargas eléctricas, robos y otra serie de amenazas. Se trata, por tanto, de **aplicar barreras físicas y procedimientos de control, como medidas de prevención y contramedidas para proteger los recursos y la información**, tanto para mantener la seguridad dentro y alrededor del Centro de Cálculo como los medios de acceso remoto a él o desde él.

Cada sistema informático es único. Por ejemplo, en la figura siguiente podemos observar la vista parcial de un CPD de una organización grande, pero en pequeñas empresas u organizaciones, el CPD podría estar compuesto solo por uno de estos módulos o por un servidor (o incluso algo parecido a lo que podemos encontrar en el `siguiente enlace <https://www.alamy.de/stockfoto-ein-computer-netzwerk-server-gehause-mit-einem-durcheinander-von-verschlungenen-blauen-kabel-166339605.html>`_).

.. raw:: html

   <div style="text-align:center;">
   <a title="Dr-text / CC BY-SA (https://creativecommons.org/licenses/by-sa/3.0)" href="https://commons.wikimedia.org/wiki/File:Datacenter-MIVITEC-MUNICH.jpg" target="_blank"><img width="256" alt="Datacenter-MIVITEC-MUNICH" src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/31/Datacenter-MIVITEC-MUNICH.jpg/512px-Datacenter-MIVITEC-MUNICH.jpg"></a>
   </div>
   </br>
   </br>
   <div style="text-align: center; color: BLUE; background-color: #e0e0e0; border-radius: 25px; padding-top: 20px;padding-right: 30px;padding-bottom: 20px; padding-left: 30px;">
   No podemos pensar que existe un plan de seguridad general aplicable a cualquier tipo de instalación.
   </div></br>

**Factores para elegir la ubicación**

Cuando hay que instalar un nuevo centro de cálculo es necesario fijarse en varios factores. En concreto, se elegirá la ubicación en función de la disponibilidad física y la facilidad para modificar aquellos aspectos que vayan a hacer que la instalación sea más segura. Existen una serie de factores que dependen de las instalaciones propiamente dichas, como por ejemplo:

.. list-table::
   :widths: 100 300
   :header-rows: 1

   * - FACTORES
     - VALORACIONES
   * - **El edificio**
     - Espacio del que se dispone,  el acceso de equipos y personal, y qué características tienen las instalaciones de suministro eléctrico, acondicionamiento térmico, etc.
   * - **Tratamiento acústico**
     - Aire acondicionado, necesarios para refrigerar los servidores, que son bastante ruidosos. Deben instalarse en entornos donde el ruido y la vibración estén amortiguados.
   * - **Seguridad física edificio**
     - Sistema contra incendios, la protección contra inundaciones y otros peligros naturales que puedan afectar a la instalación.
   * - **Suministro eléctrico CPD**
     - Condiciones especiales, ya que no puede estar sujeta a las fluctuaciones o picos de la red eléctrica que pueda sufrir el resto del edificio.
   * - **Otros Factores**
     - Existen otra serie de factores inherentes a la localización,  condiciones ambientales que rodean al local donde vayamos a instalar el CPD, factores naturales, los servicios disponibles  y otras instalaciones de la misma zona; y la seguridad del entorno(tranquilo pero no desolado).



Teniendo estos factores, y otros que se te puedan ocurrir a ti:

.. raw:: html

   <hr width=90% size=9>
**¿SABRÍAS?...**

*¿Concretar cual sería para ti una buena ubicación para un CPD en el instituto?*

.. raw:: html

   <hr width=90% size=9>

**Control de acceso**

Es necesario un férreo control de acceso al mismo. Dependiendo del tipo de instalación y de la inversión económica que se realice se dispondrá de distintos sistemas de seguridad.

.. raw:: html

   <div style="text-align:center;">
   <a title="Google Campus- Austin McKinley / CC BY (https://creativecommons.org/licenses/by/3.0)" href="https://commons.wikimedia.org/wiki/File:Google_Campus,_Mountain_View,_CA.jpg"><img width="256" alt="Google Campus, Mountain View, CA" src="https://upload.wikimedia.org/wikipedia/commons/thumb/e/e2/Google_Campus%2C_Mountain_View%2C_CA.jpg/512px-Google_Campus%2C_Mountain_View%2C_CA.jpg"></a>
   </div>
* Servicio de Vigilancia, donde el acceso es controlado por personal de seguridad.
* Detectores de Metales y escáneres de control de pertenencias.
* Utilización de **Sistemas Biométricos** ( personas cuyo acceso esté autorizado.)
* Protección Electrónica, basada en el uso de sensores conectados a centrales de alarma que reaccionan ante la emisión de distintas señales.

Una de los apartados más importantes en cuanto a la seguridad física de nuestros equipos es todo lo que tiene que ver con el suministro eléctrico, el cual vamos a estudiar con más detenimiento en el siguiente apartado.

.. toctree::
   :maxdepth: 1

   Electrica
