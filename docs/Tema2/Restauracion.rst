Puntos de Restauración
==============================
Casi todos los SO (La familia de Windows p.e) permiten crear puntos de restauración (cada vez que se apaga correctamente se guarda uno tb) para poder regresar a un estado anterior en caso de problemas, de este modo podemos regresar a una configuración estable. No se trata de una reinstalación del sistema, solo los archivos modificados son restaurados a como estaban cuando se creo el punto de restauración.

.. image:: img/puntorestauracionwin.png
    :width: 400 px
    :alt: Punto restauración Windows
    :align: center

Una de las múltiples ventajas de la virtualización, ya sea a nivel de virtualización de SO completo (VMWare, VirtualBox..), como de contenedores (Docker, LXC…), ES LA POSIBILIDAD DE CREAR INSTANTÁNEAS(SNAPHOTS). Éstas nos permiten ’regresar’ a ese estado en cualquier momento. Se caracterizan por su rápidez de creación y restauración  por lo que son ideales para servers.

.. image:: img/snapshotvbox.png
    :width: 400 px
    :alt: backup incremental
    :align: center

Destacar que **LVM** maneja Snapshots, por lo que tener una 'copia de seguridad' de nuestros sistemas se hace muy sencillo. Puedes consultar un manual bastante sencillo en el `siguiente enlace <https://www.tecmint.com/take-snapshot-of-logical-volume-and-restore-in-lvm/>`_.

.. raw:: html

    </br>
    <div style="text-align: justify; color: orange; background-color: #e0e0e0; border-radius: 25px; padding-top: 20px;padding-right: 30px;padding-bottom: 20px; padding-left: 30px;">
    <u><b>PRÁCTICA 6</b></u></br>
    Realiza la práctica 6 del Tema 2 del aula virtual, vas a realizar una recuperación de tu sistema tanto en Windows como en Linux utilizando distintas herramientas.
    </div>
