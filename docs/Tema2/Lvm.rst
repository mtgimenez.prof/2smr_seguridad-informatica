LVM
====

El **Gestor de Volúmenes Lógicos (LVM)**, es un sistema de gestión del almacenamiento que ofrece múltiples ventajas. Básicamente nos va a permitir mejorar la administración de nuestros discos y sus particiones, agrupándolos como mejor nos interese a nivel lógico, independientemente de sus características y ubicaciones físicas. Para entender qué posibilidades nos ofrece LVM debemos tener presentes tres conceptos que se manejan en estas estructuras:

    1. **Volumen Físico (PV)**: Soporte físico de almacenamiento, habitualmente un disco o partición. Aunque podemos agrupar varios elementos, y de tipos muy variados (RAID, SAN..)
    2. **Grupo de Volúmenes (VG)**: Agrupación lógica de 1 o varios PV.
    3. **Volumen Lógico (LV)**: Una porción lógica de un VG.

      .. image:: img/estructuraLvm.png
          :width: 500 px
          :alt: Estructura LVM
          :align: center
          :target: https://github.com/miztiik/AWS-Demos/tree/master/How-To/setup-lvm-in-EC2

    Puedes encontrar más ayuda en el este `Tutorial sobre la creación y gestión de LVM <https://blog.inittab.org/administracion-sistemas/lvm-para-torpes-i/>`_, o en el siguiente video:

    .. raw:: html

              <iframe width="250" style="display:block; margin-left:auto; margin-right:auto;" src="https://www.youtube.com/embed/dMHFArkANP8" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


    **VOLÚMENES FÍSICOS**

    Con el comando `pvcreate <https://linux.die.net/man/8/pvcreate>`_, podemos crear PV, añadiendo discos enteros o particiones, teniendo en cuenta algunos aspectos:

      * **Si se crean sobre particiones**: Éstas deben ser lógicas(identificadas por el tipo 0x8e).

          .. image:: img/pvCreate.png
              :width: 400 px
              :alt: pvcreate
              :align: center

      * **Si se crean sobre discos**: Debe tener la tabla de particiones borrada, mediante un programa tipo gparted, o con un comando como (si queremos usar el segundo disco SATA).
                .. code-block:: shell-session

                     #dd if=/dev/zero of=/dev/sdb bs=512 count=1

      * El uso del comando es muy sencillo, únicamente hace falta indicar el dispositivo que queremos asignar al volumen físico que estoy creando:
                .. code-block:: shell-session

                     #pvcreate /dev/sdc4

      * Para visualizar los PV accesibles en el sistema, tenemos el comando pvdisplay, el cual, además nos muestra bastante información del volumen
                .. code-block:: shell-session

                     #pvdisplay /dev/sdc4

      * En caso de error al crear el PV (	Device /dev/sdb1 not found (or ignored by filtering)):
                .. code-block:: shell-session

                     #partprobe /dev/sdb



    .. image:: img/pvCreateresult.png
              :width: 400 px
              :alt: pvcreate
              :align: center

    **GRUPOS DE VOLÚMENES**

      * Para poder crear grupos de volúmenes (VG) necesitamos, al menos un volúmen físico (PV). Para ello tenemos el comando `vgcreate <https://linux.die.net/man/8/vgcreate>`_. Si utilizamos dispositivos que no han sido inicializados como PV con pvcreate, el comando vgcreate se encargará de hacerlos PV antes de incluirlos en el VG.

              .. code-block:: shell-session

                    #vgcreate Volumen_1 /dev/sdc4 /dev/sdd4 ....

      * Para visualizar los VG accesibles en el sistema, tenemos el comando vgdisplay, el cual, además nos muestra bastante información del volumen

    .. image:: img/vgCreateresult.png
              :width: 400 px
              :alt: pvcreate
              :align: center


    **VOLÚMENES LÓGICOS**

      * Los volúmenes lógicos son una sección de un grupo de volúmenes. El comando `lvcreate <https://linux.die.net/man/8/lvcreate>`_ crea un nuevo volumen lógico (LV) en un grupo de volúmenes (VG) asignando extensiones lógicas (LE), que se crean desde las extensiones físicas (PE) libres de ese grupo de volúmenes. Esto nos permite:

          1. **Ampliar un LV** mientras queden PE libres en el VG (con el comando `lvextend <https://www.linuxtechi.com/extend-lvm-partitions/>`_).
          2. **Reducir un LV**, liberando PE que pueden ser utilizados por otros LV del VG (con el comando `lvreduce <https://www.linuxtechi.com/reduce-size-lvm-partition/>`_).
      * Para crear un LV, debemos indicarle el VG al que pertenece, el tamaño (en PEs con la opción -l o en megas/gigas/teras con -L) y optionalmente, el nombre que queremos darle (-n). **Es importante asegurarse primero que tenemos espacio disponible en el VG que vayamos a utilizar**

              .. code-block:: shell-session

                    //Un LV de 40MB en el VG Volumen_1 para guardar música
                    #lvcreate -L 40M -n musica Volumen_1

                    // O especificando el número de PEs (de 4MB por defecto)
                    #lvcreate -l 10 -n musica Volumen_1

      * Una vez creado, la forma de referirse al LV (**para crear y montar** el sistema de ficheros..) será: **/dev/NOMBRE_DEL_VG/NOMBRE_DEL_LV**. (*/dev/Volumen_1/musica* en el ejemplo)

      .. image:: img/lvCreateresult.png
                :width: 400 px
                :alt: Mfks to the LVM
                :align: center


      * Si mostramos el estado de nuestro LV, podemos ver algunos detalles interesantes. Para ello tenemos el comando **lvdisplay** con la opción -m.

      .. image:: img/lvDisplay.png
                :width: 400 px
                :alt: Mfks to the LVM
                :align: center

      * Despúes podemos realizar los pasos necearios para hacer nuestro LV accesible como cualquier otro dispositivo de bloques, recuerda:

          1. Crear el sistema de ficheros en el dispositivo.

              .. image:: img/lvMkfs.png
                        :width: 400 px
                        :alt: Mfks to the LVM
                        :align: center

          2. Montar el sistema de ficheros en el directorio que queramos, **con el comando mount o a través del fichero /etc/fstab**.

      * Vamos a comprobar la mayor utilidad de LVM. Tal y como se ha comentado antes, **la principal ventaja de los volúmenes lógicos es que son dispositivos DINÁMICOS, los cuales pueden ser redimensionados fácilmente**. Las modificaciones en el tamaño de los LV dependen del espacio existente en su VG y en los PV que lo componen, la dinámica en este caso sería siempre algo parecido a la siguiente imagen.

              .. image:: img/lvMetodologia.png
                        :width: 225 px
                        :alt: Mfks to the LVM
                        :align: center

              .. code-block:: shell-session

                    #df -h /mnt
                    #pvcreate /dev/sdd1
                    #vgextend volumen_1 /dev/sdd1
                    #lvextend -L+1G /dev/volumen_1/musica
                    #resize2fs /dev/volumen_1/musica
                    #df -h /mnt

              .. image:: img/lvDisplayResultExtend.png
                        :width: 400 px
                        :alt: Extending the LVM
                        :align: center

En la web hay cantidad de recursos(manuales, videotutoriales...) que explican como manejar LVM, aquí puedes encontrar un video muy completo, y con toda la documentación necesaria a través del `siguiente enlace <https://christitus.com/lvm-guide/>`_.

.. raw:: html

        </br>
        <div style="text-align: justify; color: orange; background-color: #e0e0e0; border-radius: 25px; padding-top: 20px;padding-right: 30px;padding-bottom: 20px; padding-left: 30px;">
        <u><b>PRÁCTICA 4</b></u></br>
        Realiza la práctica 4 del Tema 2 del aula virtual, comprobarás la flexibilidad y la simplicidad de los LVM.
        </div>
