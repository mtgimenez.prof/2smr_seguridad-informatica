Sistemas de Almacenamiento Externo
==================================
Existen alternativas al almacenamiento habitual que nos permitirán un control y una gestión mucho mayores sobre los datos procesados, como las tecnologías NAS y SAN, que pueden utilizarse junto con los clusters. El objetivo es permitir la ***compartición de recursos, la alta disponibilidad y evitar las pérdidas de información**.

**A)NAS (Network Attached Storage)**

Los dispositivos NAS (Network Attached Storage) son dispositivos de almacenamiento a los cuales se accede utilizando protocolos de red de la pila TCP/IP.  Los sistemas NAS suelen estar compuestos por uno o más dispositivos que se disponen en RAID, lo que permite aumentar su capacidad, eficiencia y tolerancia ante fallos. Utilizan la red local para el envío de información.
Cualquier equipo que comparta una carpeta por alguno de los protocolos existentes, ya sería un NAS. Alternativas para compartir archivos:

* SAMBA
* NFS
* FreeNAS
* FTP
* ……

.. image:: img/nas.png
   :width: 300 px
   :alt: NAS Diagram
   :align: center


**B) SAN (Storage Area Network)**

Una "SAN" (Red de área de almacenamiento) es una red de almacenamiento integral que agrupa los siguientes elementos:

* Una red de alta velocidad
* Un equipo de interconexión dedicado
* Elementos de almacenamiento de red (DD)

Es una red dedicada al almacenamiento que está conectada a las redes de comunicación de una compañía. Además de contar con interfaces de red tradicionales, los equipos con acceso a la SAN tienen una interfaz de red específica que se conecta a la SAN.

.. raw:: html

   <div style="text-align:center;">
   <a title="Michael Moll + Seekater / GFDL (http://www.gnu.org/copyleft/fdl.html)" href="https://commons.wikimedia.org/wiki/File:Schema_SAN_german_V2.png"><img width="512" alt="Schema SAN german V2" src="https://upload.wikimedia.org/wikipedia/commons/7/7f/Schema_SAN_german_V2.png"></a>
   </div>

**C)Almacenamiento en la nube(Cloud Storage)**

.. raw:: html

    <p>
      El Almacenamiento en la Nube consiste en guardar archivos en un lugar de Internet. Esos lugares de Internet son aplicaciones o servicios que almacenan o guardan esos archivos. Los archivos pasan de estar en nuestros dispositivos a estar guardados en ese servicio o aplicación (fuente: DATAPRIUS).<sup id="fnref:note1"><a class="footnote-ref" href="#fn:note1" role="doc-noteref">1</a></sup>
    </p>

.. image:: img/comparativalmacenamientonube.png
   :width: 300 px
   :alt: NAS Diagram
   :align: center

Actualmente se ofrecen servicios mediante los cuales podríamos montar nuestra propia nube de almacenamiento. Un ejemplo podría ser NextCloud (`ver video en Youtube <https://youtu.be/vULDXpOJOUg>`_ )

.. raw:: html

   </br>
   <div class="footnotes">
       <hr />
       <ol>
           <li class="footnote" id="fn:note1">
               <p>
                   <b>Fuente:</b> <a href="https://blog.dataprius.com/index.php/2016/08/03/almacenamiento-en-la-nube/" target="_blank">Dataprius</a> <a class="footnote-backref" rev="footnote" href="#fnref:note1">&#8617;</a>
               </p>
           </li>
       </ol>
   </div>
