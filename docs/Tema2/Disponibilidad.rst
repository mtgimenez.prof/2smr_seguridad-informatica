=========================
Almacenamiento Redundante
=========================
Los **objetivos** que se persiguen con la configuración de un sistema de almacenamiento redundante son:

* **Mayor capacidad**: es una forma económica de conseguir capacidades grandes de almacenamiento. Combinando varios discos más o menos económicos podemos conseguir una unidad de almacenamiento de una capacidad mucho mayor que la de los discos por separado.
* **Mayor tolerancia a fallos**: en caso de producirse un error, con el uso de un RAID, el sistema será capaz, en algunos casos, de recuperar la información perdida y podrá seguir funcionando correctamente.
* **Mayor seguridad (disponibilidad)**: debido a que el sistema es más tolerante a los fallos y mantiene cierta información duplicada, aumentaremos la disponibilidad y tendremos más garantías de la integridad de los datos.
* **Mayor velocidad (rendimiento)**: al tener en algunos casos cierta información repetida y distribuida, se podrán realizar varias operaciones simultáneamente, lo que redundará en una mayor velocidad de acceso.
* **Mayor flexibilidad**: ante el previsible llenado de nuestros dispositivos de almacenamiento tras un tiempo de uso, nuestro sistema debería ofrecernos alternativas fáciles para poder ampliar su espacio.

Para ello vamos a estudiar 2 alternativas:


.. toctree::
   :maxdepth: 1

   Raid
   Lvm


.. raw:: html

    <div style="text-align: center; color: BLUE; background-color: #e0e0e0; border-radius: 25px; padding-top: 20px;padding-right: 30px;padding-bottom: 20px; padding-left: 30px;">
    Los RAID se utilizan para conseguir redundancia y por tanto disponibilidad. La característica principal de los Volúmenes Lógicos es que son DINÁMICOS, lo que nos permite ampliar o reducir su capacidad a voluntad. 
    </div></br>
