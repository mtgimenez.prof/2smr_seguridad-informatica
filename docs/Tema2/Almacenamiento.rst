Almacenamiento de la información
================================
Otro de los factores clave de la seguridad de cualquier sistema es cómo y donde se almacena la información. Hablaremos de tres aspectos importantes: **Rendimiento, la disponibilidad y la accesibilidad**.

.. raw:: html

   <div style="text-align:center;">
   <a title="Kerrick Staley / CC BY-SA (https://creativecommons.org/licenses/by-sa/3.0)" href="https://commons.wikimedia.org/wiki/File:Memory_hierarchy_diagram.jpg"><img width="256" alt="Memory hierarchy diagram" src="https://upload.wikimedia.org/wikipedia/commons/f/f6/Memory_hierarchy_diagram.jpg"></a>
   </div>

* **Rendimiento**: Capacidad de disponer un volumen de datos en un tiempo determinado. Se mide en tasa de transferencia(MBps). Deben tenerse en cuenta factores como:
    * Coste por Bit
    * Tiempo de accceso
    * Capacidad(tamaño)
* **disponibilidad**: Seguridad que la información pueda ser recuperada en el momento en que se necesite:
    * Redundancia de la información
    * Distribución de la información
* **Accesibilidad**: La información estará disponible lo más fácilmente posible a aquellos usuarios con acceso autorizado:
    * Seguridad
    * Facilidad de acceso

Actualmente existen multitud de alternativas en cuanto a los **tipos de unidades de almacenamiento** disponibles, cada una de ellas presenta unas características distintas de fiabilidad, velocidad y coste:

  1. HDD
  2. SSD
      * SATA
      * mSATA
      * M.2
  3. SCSI

Además de la tecnología a utilizar, otro de los aspectos fundamentales hoy en día acerca de la gestión del almacenamiento es la **UBICACIÓN** del mismo. En este sentido tenemos varias alternativas que se analizan en el siguiente apartado. Además también tenemos distintas alternativas para organizar nuestro sistema de almacenamiento con estructuras que nos proporcionen **ALTA DISPONIBILIDAD**.


.. toctree::
   :maxdepth: 1

   AlmacenamientoExterno
   Disponibilidad
