Hardware y Gestión del almacenamiento
=======================================

En este tema vamos a tratar con la seguridad que compromete al equipamiento de nuestro sistema en alguna de sus facetas. Empezaremos hablando sobre la **SEGURIDAD FÍSICA**, la cual se encarga de la integridad del Hardware, destacando un apartado específico para la **Seguridad Eléctrica**, dada su importancia.
En los dos últimos apartados estudiaremos la **gestión del almacenamiento**, las opciones que tenemos para proveer a nuestro SI de unas características de Funcionamiento que puede ayudarnos a mejorar nuestra calidad de vida como *sysadmins*.

Puedes observar que, en todos los apartados vamos a analizar técnicas y herramientas de **SEGURIDAD PASIVA**. Encargada de recuperar el sistema lo mejor posible tras un ataque/fallo.

Los **OBJETIVOS** de este tema son:

#. Valorar criterios para mejorar la seguridad Física de los SI.
#. Conocer conceptos y herramientas relacionados con la seguridad eléctrica.
    * SAI
#. Conocer la gestión de almacenamiento y estructuras disponibles para mejorar la disponibilidad.
    * NAS/SAN/Cloud
    * RAID/LVM
#. Gestionar y programar sistemas de recuperación.
    * Backups
    * Imágenes
    * Ptos. de restauración

Para ello vamos a analizar los siguientes apartados:

.. toctree::
   :maxdepth: 3

   Fisica
   Almacenamiento
   Recuperacion
