Seguridad Lógica
===================
Todavía existen muchas herramientas que, a nivel local, pueden ayudarnos a administrar nuestras instalaciones, especialmente servidores u equipos críticos para el funcionamiento
de nuestro sistema informático.

Monitorización del sistema
---------------------------
Una vez que tenemos todas las medidas anteriores implantadas, **¿Ya hemos acabado? No**, debemos vigilar que todo funciona correctamente. **¿Cómo podemos vigilar?**

* Revisando los **logs** del sistema y las aplicaciones para detectar sucesos anómalos o demasiado frecuentes que puedan suponer un problema.
* Activar la copia sincronizada otra máquina para tenerla duplicada
* Suscribirse a **Blogs, newsletters, RSS..** de proveedores o de temáticas de interés (SSOO, adm. De sistemas..)
* Participar en foros de usuarios para encontrar solución a problemas que ya han resuelto otras personas.

**WINDOWS**

Podemos abrir el visor de sucesos mediante la orden **eventvwr.msc**. Se guarda información de los sucesos de aplicación, seguridad y sistema. Se puede  configurar el tamaño y el acceso a los mismos. Podemos generar vistas personalizadas.

      .. image:: img/visorEventosWindows.png
                :width: 400 px
                :alt: Visor de eventos en Windows
                :align: center

.. warning::

        Crea un filtro de los eventos personalizado para algún nivel de incidente y algún tipo en concreto o de algún rango de tiempo.

**LINUX**

Linux tiene un complejo visor de archivos sucesos. Puede no estar instalado en Ubuntu server. Existe una estructura de log para todas las aplicaciones que tenemos en el sistema. Linux nos da características, como:

* Facil acceso/modificación de la ubicación.
* Uso de tuberias.
* Facil creacion de scripts.

Puedes encontrar un `completo tutorial sobre log en Linux en la web <https://geekland.eu/logs-en-linux/>`_.

.. warning::

        Crea un comando que muestre los servicios que han fallado al iniciarse en el arranque del sistema.


Actualizaciones
---------------
Los Sistemas Operativos requieren de actualizaciones periódicas, por varios motivos:

* Actualizaciones hardware: Debido a que el hardware de las máquinas evoluciona, es necesario crear programas capaces de gestionar este nuevo hardware.
* Actualizaciones de los programas/SSOO: En ocasiones, se detectan vulnerabilidades o fallos en los programas que son subsanados en posteriores actualizaciones.
* Nuevas funcionalidades: Con frecuencia, los sistemas operativos incorporan nuevas funcionalidades que los usuarios pueden aprovechar descargándoselas en las actualizaciones.

.. warning::

        Hay que tener algunos aspectos en cuenta si hablamos de actualizaciones de Software (sobretodo del sistema operativo):

        * Distintas herramientas dependiendo de la distribución en Linux. **¿Sabrías usar el sistema correspondiente en otras distribuciones?**
        * ServicePack
        * LTS - Debian/Ubuntu
        * ELS - RedHat
        * **Estable vs Actualizado**

Orígenes del Sw
---------------
Como futuros administradores de sistemas debemos tener en cuenta que un servidor que esté proporcionando uno o varios servicios, debe tener dos características básicas:

1. Estabilidad
2. Seguridad

Posiblemente, si hablamos de software, sean dos conceptos un poco conflictivos entre sí, ya que si para lograr la estabilidad procuramos versiones de SO o software probadas, la seguridad
suele empezar por mantener actualizados esos programas.

Hay que encontrar un **equilibrio entre ambas estrategias (actualizar/mantener)**. Usando algunos conceptos que minimicen nuestros riesgos:

* Entornos de prueba (para probar actualizaciones)
* Copias de seguridad (por si todo falla)
* **Orígenes de software fiables.**

Este último punto es el que nos ocupa, si bien tiene mas presencia con los sistemas Linux, los cuales se manejan con gestores de paquetes (`info en la web <https://es.wikipedia.org/wiki/Sistema_de_gesti%C3%B3n_de_paquetes>`_) y repositorios (**apt, yum, ...**). También en Windows comienzan a funcionar elementos similares.

.. note::

        Que repositorios tenemos por defecto en ubuntu 22.04. ¿Donde los encuentro?¿Cómo los puedo gestionar? ¿Qué significan las palabras finales?¿Qué pasaría si modifico esos repositorios?¿Es seguro añadir nuevos?



Herramientas de protección
--------------------------

**ANTIVIRUS**

En informática los antivirus son programas cuyo objetivo es detectar y/o eliminar virus informáticos. Con el transcurso del tiempo, la aparición de sistemas operativos más avanzados e Internet, ha hecho que los antivirus hayan evolucionado hacia programas más avanzados que no sólo buscan virus informáticos, sino también otros tipos de peligros.

**CONGELADORES DE DISCO**

Dada la abundancia de dispositivos externos en los cuales que almacenar la información, una alternativa puede ser la utilización de los CONGELADORES DE DISCO. De manera que trabajas normalmente con él (crear y borrar archivos, instalar y desinstalar programas, modificar el aspecto del escritorio, etc) pero cuando arranques de nuevo, ningún cambio habrá tenido efecto, es decir: el disco duro tendrá exactamente el mismo contenido que al principio. Algunos SO ya implementan esta funcionalidad.

* `Unified Write Filter (UWF) <https://learn.microsoft.com/es-es/windows/configuration/unified-write-filter/>`_ → windows 10 (`Manual en la web <https://docs.microsoft.com/es-es/windows/iot-core/secure-your-device/unifiedwritefilter#how-to-use-uwf>`_)
* `Deep Freeze <https://www.faronics.com/es/products/deep-freeze>`_

.. image:: img/uwfWindows.png
                :width: 300 px
                :alt: Visor de eventos en Windows
                :align: center
