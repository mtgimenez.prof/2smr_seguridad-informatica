Seguridad en el Sistema Operativo
==================================

Los aspectos a tratar de la seguridad FÍSICA de acceso al ordenador(ubicación, sistemas biométricos..) ya se han tratado temas anteriores. El tema actual y el siguiente se centran en la **SEGURIDAD LÓGICA**.

La calidad de un plan de seguridad informática para un sistema puede analizarse desde dos perspectivas:

1. Seguridad en la red: Qué dispositivos y políticas se han implementado para asegurar la LAN en la que puedan encontrarse los equipos.
2. **Seguridad LOCAL**: Qué mecanismos de protección podemos utilizar en nuestro equipo informático para evitar accesos indeseados de intrusos (personas o programas informáticos).

Este tema se centra el el estudio de la SEGURIDAD LOCAL, prestando especial atención a las herramientas que pueden proporcionarnos los Sistemas Operativos y a las aplicaciones, para aumentar la seguridad de un sistema informático, mejorando además las
capacidades de gestion y control del funcionamiento..

.. toctree::
   :maxdepth: 2

   acceso
   seglogica
