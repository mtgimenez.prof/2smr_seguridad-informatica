Politicas de acceso
===================
Vamos a estudiar algunas opciones que nos brindan los SSOO actuales para controlar el acceso y el uso que l@s usuari@s puedan hacer de los recursos que ofrecemos, siempre desde una perspectiva LOCAL al Sistema Informático.

Contraeñas
-----------
Una de los principales aspectos que ha de tratar una política de seguridad es la POLÍTICA DE CONTRASEÑAS(cuestiones como la complejidad o la durabilidad), en cuanto al método de autentificación que proporciona el **SO ó protocolo de autentificación en red**, que estemos utilizando.

    .. Warning::
        ¿Sabrías poner 2 ejemplos de protocolos de identificación en red?

Sin embargo, existen otros niveles de seguridad que podemos proteger con contraseña, por ejemplo:
    * BIOS.
    * Gestor de arranque.

**CONTRASEÑA EN LA BIOS**

Dependiendo del modelo de placa base el acceso a la BIOS se realizará de un modo u otro, así como la configuración de la contraseña en ella.  Puede haber 2 contraeñas a configurar en una BIOS:

    A. Contraseña de administrador: De acceso a la BIOS.
    B. Contraseña de usuario: Para iniciar el ordenador.

    .. Warning::
        ¿Que contraseña te parece más importante, la A  ó la B?¿Como intentarías acceder al PC en caso de olvidar la contraseña que tiene la BIOS?

**CONTRASEÑA EN GESTOR DE ARRANQUE**

El gestor de arranque que por defecto instalamos con cualquier versión de Ubuntu/Debian es GRUB. Para evitar que personas no autorizadas tengan acceso a la edición de las opciones de arranque de los distintos sistemas operativos que controla el GRUB, estableceremos una contraseña.

Podemos establecer contraseñas en GRUB a 2 niveles:

    1. Impedir el acceso a las opciones de GRUB que podemos modificar al arrancar el ordenador.
    2. Proteger los SO de accesos no autorizados.

Con la configuración de contraseñas en el gestor de arranque algo relativamente sencillo, tal y como muestra `este manual <https://geekland.eu/proteger-el-grub-con-contrasena/>`_, podemos ahorrarnos accesos a nuestro sistema en los que se modifiquen configuraciones de usuario
aprovechando algunas funcionalidades propias de estos programas, accede al `siguiente tutorial <https://blog.desdelinux.net/reset-password-root-grub/>`_ para ver un ejemplo.


  .. Warning::

          ¿Sabrías realizar la configuración de contraseña en el gestor de arranque GRUB para evitar modificaciones de las opciones y accesos no autorizados?

**GESTORES DE CONTRASEÑAS**

Un gestor de contraseñas es un programa que se utiliza para almacenar una gran cantidad de parejas usuario/contraseña. La base de datos donde se guarda esta información está **cifrada mediante una única clave (contraseña maestra o en inglés master password)**,
de forma que el usuario sólo tenga que memorizar una clave para acceder a todas las demás.
Esto facilita la administración de contraseñas y fomenta que los usuarios escojan claves complejas sin miedo a no ser capaces de recordarlas posteriormente.

    * Los **navegadores** incorporan un gestor de contraseñas que se puede proteger con una contraseña maestra.
    * **Aplicaciones** independientes que tienen el mismo cometidos son más seguras y completas.
    * Existen gestores de contraseñas online que permiten gestionar gran cantidad de cuentas de forma remota.

    .. image:: img/gestorcontrasenas.png
        :width: 300 px
        :alt: Gestor contraseñas GUI Linux
        :align: center

    .. Warning::

        1. ¿Sabrías encontrar ejemplos de gestores de contraseñas **para Windows y Linux**?
        2. Encuentra algún **gestor en linea**, averiguando el **coste** asociado.
        3. ¿En general crees que **son seguras** este tipo de herramientas?
        4. ¿Usas los que incorporan los **navegadores** actuales?

Cifrado de información.
------------------------

Además de en una comunicación, podemos utilizar las herramientas de cifrado aplicándolas a estructuras de información de nuestros dispositivos de almacenamiento:

    * Directorios
    * Particiones
    * Discos

Existen multitud de opciones para encriptar unidades de almacenamiento, por ejemplo:

    A. Integradas en el S.O:
        * Windows (En las últimas versiones, viene incorporado en el propio SO con la `utilidad BitLocker <https://support.microsoft.com/es-es/windows/cifrado-de-dispositivo-en-windows-10-ad5dcf4b-dbe0-2331-228f-7925c2a3012d>`_)
        * Linux (`Sistema de ficheros ecryptfs <https://www.solvetic.com/tutoriales/article/2936-como-encriptar-directorios-con-ecryptfs-en-linux/>`_)
            .. tip::
                  Intenta encriptar una carpeta creada por ti con información importante
    B. Aplicaciones externas → `gnome-disk-utility con cryptsetup <https://www.neowin.net/news/how-to-create-encrypted-partitions-on-linux-with-gnome-disks/>`_. Nos permitirá encriptar dispositivos, entre otras cosas.
    C. Plugins → Seahorse, el gestor de contraseñas y el gestor de ficheros Nautilus (`ver manual <https://ubunlog.com/seahorse-cifra-datos-escritorio/>`_). La app viene instalada en Ubuntu
    D. Virtualización → Los sistemas de virtualización lo incorporan (`p.e VirtualBox <https://donnierock.com/2018/09/26/cifrar-una-maquina-virtual-de-virtual-box/>`_).

    .. Warning::
        Imagina algún escenario en el que sería de utilidad realizar esto en los equipos.

.. raw:: html

    </br>
    <div style="text-align: justify; color: orange; background-color: #e0e0e0; border-radius: 25px; padding-top: 20px;padding-right: 30px;padding-bottom: 20px; padding-left: 30px;">
    <u><b>Ejercicio 1</b></u></br>
    Realiza el ejercicio 1 del Tema 4 del aula virtual. Encriptación de particiones en  Ubuntu.
    </div>

Permisos
---------

.. raw:: html

    <p>
    Los SSOO modernos permiten asignar permisos (o derechos de acceso) a los archivos para determinados usuarios y grupos. De esta manera, se puede restringir o permitir el acceso a un
    directorio/archivo para su visualización de contenidos, modificación y/o ejecución.
    Estas capacidades se han ido mejorando hasta poder establecer distintos ’niveles’ de asignación de permisos de acceso<sup id="fnref:note1"><a class="footnote-ref" href="#fn:note1" role="doc-noteref">1</a></sup>:
    </p>


1. **Discretionary Access Control (DAC)**: Control de acceso discrecional. Son, por ejemplo, los permisos de Linux que ya conocemos (chmod...). Los cuales pueden ampliarse con elementos como:
    * **Access Control List (ACL)**: Listas de control de acceso.
    * Bits especiales: **SUID, SGID, Sticky Bit**..

    .. raw:: html

        <p style="text-align: justify;"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/4/42/Pdf-2127829.png/480px-Pdf-2127829.png" alt="Perfil" width="50" style="vertical-align: middle; float:left;"/>En las Utilidades del Aula virtual tienes un manual de este tipo de permisos.</br> </br>

    .. image:: img/permisos.pdf
          :width: 400 px
          :alt: Gestión básica de los permisos de acceso.
          :align: center


2. **Mandatory Access Control.(MAC)**: Permite delimitar qué puede ejecutar un proceso en su ejecución (SELinux en redhat, AppArmor en Ubuntu..) → ¿Vemos algún ejemplo?
3. **Role Base Access Control(RBAC)**: Control de acceso basado en roles. Lo implementan algunos SO.

.. raw:: html

    <p>
      Una funcionalidad de los SO, es la de <b>elevación de privilegios</b>, con la cual podemos hacer algo como si fuéramos otro usuario.
      ¡¡Esto puede ser potencialmente peligroso!!<sup id="fnref:note1"><a class="footnote-ref" href="#fn:note2" role="doc-noteref">2</a></sup>
    </p>



.. note::

      * Windows: **UAC**
      * Ubuntu(Linux): **sudo/sudoers** → ¿Sabrías modificar el **sudoers** para que solo te dejara algunos comandos? ¿Y para que no te pida contraseña?
      * Linux: **umask**

.. raw:: html

    </br>
    <div style="text-align: justify; color: orange; background-color: #e0e0e0; border-radius: 25px; padding-top: 20px;padding-right: 30px;padding-bottom: 20px; padding-left: 30px;">
    <u><b>PRÁCTICA 1</b></u></br>
    Realiza la práctica 1 del Tema 4 del aula virtual. Gestión de los permisos DAC en Windows y Linux.
    </div>

Cuotas
-------

Los SSOO poseen mecanismos para impedir que los usuarios hagan un uso indebido de la capacidad del disco, y así evitar la ralentización del equipo por saturación del sistema de ficheros
y el perjuicio al resto de los usuarios. A estas configuración se le denomina **CUOTA DE DISCO** o simplemente **CUOTAS**.

.. note::

      Los sistemas de cuotas NO SE CONFIGURAN SOBRE DIRECTORIOS, sino sobre sistemas de ficheros (esas lineas que incluimos en el fichero */etc/fstab* ¿Recuerdas?):

        * Particiones.
        * RAID
        * LVM

**WINDOWS**

    * Clic derecho **sobre la partición → Propiedades → Cuota**
    * Si solo queremos un seguimiento no activamos Denegar espacio de disco a usuarios ….
    * Si queremos limitar el espacio, debemos definir la capacidad de disco y el nivel de advertencia y activar la opción anterior.
    * Si se marcan las opciones del final, el SO registraría los eventos de superación(limite  o  advertencia.)
    * En valores de cuota podemos comprobar el estado actual del uso de cuota de los usuarios

.. image:: img/cuotaWindows.png
          :width: 300 px
          :alt: Gestión cuotas en Windows
          :align: center

.. warning::

      ¿Sabrías montar un sistema de cuotas en Windows, en una de tus MV?¿Y acceder al visor de eventos para comprobar los registros generados? *¿Quizá creando una vista personalizada?*

**LINUX**

    * Suelen aplicarse cuotas sobre el directorio /home (información de los usuarios)
        * Para ello /home se instala en una partición aparte.
    * Es probable que haya que instalar algún paquete.
    * Diferentes opciones de gestión:
        * Directamente con el terminal.(`Tutorial en la web <https://somebooks.es/instalar-y-configurar-cuotas-de-disco-en-ubuntu-server-20-04-lts/>`_)

        .. raw:: html

                   <div style="text-align: center;">
                   <iframe width="280" height="157" src="https://www.youtube.com/embed/psX9a6w0aOw?si=4uB575JUyjHu-Kta" frameborder="0"
                   allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                   </div>

        * Utilizar interfaz de administración web como webmin, teniendo en cuenta los riesgos en cuanto a la seguridad, facilita el proceso(`manual de instalación <https://tecnosender.com/instalar-webmin-en-ubuntu-24-04/>`_). Una vez instalado se puede `consultar la documentación oficial del módulo de cuotas en Webmin <https://webmin.com/docs/modules/disk-quotas/#introduction-to-disk-quotas>`_

        .. raw:: html

                   <div style="text-align: center;">
                   <iframe width="280" height="157" src="https://www.youtube.com/embed/maD9cFYindU" frameborder="0"
                   allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                   </div>


Chroot
--------

En los sistemas Unix una jaula chroot, es una operación que invoca un proceso, cambiando para este y sus hijos el directorio raíz del sistema. Se llama así por el nombre del comando que se usa para crearla:

      .. code-block:: shell-session

                     #chroot [--userspec=abel] /home/abel [/bin/bash]

A tener en cuenta:
    * Pueden crearse jaulas para usuarios|grupos específicos.
    * **Debe hacerse accesibles todos los elementos que necesite el programa para ejecutarse(DIRECTORIOS, LIBRERÍAS...)**

.. image:: img/jaulachroot.png
          :width: 400 px
          :alt: Gestión cuotas en Windows
          :align: center

¿Que usos puede tener?
    * Olvido de contraseña: manual en la web: `tut. online <https://netosec.com/reset-linux-root-password/>`_
    * Errores en la configuración del gestor de arranque (grub): `manual online <https://www.enmimaquinafunciona.com/pregunta/26846/como-ejecutar-update-grub-desde-un-livecd>`_
    * Evitar accesos a nuestro sistema de ficheros por parte de programas públicos, como servidores ejecutándose en nuestro sistema (**Apache, FTP, DNS..**). Puedes encontrar un manual donde lo hace para SSH en https://www.tecmint.com/restrict-ssh-user-to-directory-using-chrooted-jail/
  


.. raw:: html

   </br>
   <div class="footnotes">
       <hr />
       <ol>
           <li class="footnote" id="fn:note1">
               <p>
                   <b>Puedes encontrar una explicación más detallada en:</b> <a href="https://www.incibe-cert.es/blog/control-acceso" target="_blank">https://www.incibe-cert.es/blog/control-acceso</a> <a class="footnote-backref" rev="footnote" href="#fnref:note1">&#8617;</a>
               </p>
           </li>
           <li class="footnote" id="fn:note2">
               <p>
                   Ejecutar siempre tus comandos con <b>sudo</b> puede crearte problemas<a class="footnote-backref" rev="footnote" href="#fnref:note2">&#8617;</a>
               </p>
           </li>
       </ol>
   </div>
